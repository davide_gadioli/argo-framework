#ifndef ARGO_OP_PARSER_H
#define ARGO_OP_PARSER_H


#include <string>
#include <list>
#include <set>


#include "config.h"
#include "monitor.h"
#include "goal.h"
#include "asrtm.h"
#include "operating_point.h"
#include "awm.h"




class Parser
{
public:
	Parser();
	
	void ParseConfiguration(std::string xml_file);
	void ParseOps(std::string xml_file);
	void ParseOps(std::string xml_file, std::string post_file);
	void ParseAwms(std::string xml_file);
	
	
	void BuildFiles(std::string out_header, std::string out_src);
	
	
	
private:
	
	bool with_config;
	MonitorList monitor_list;
	GoalList goal_list;
	Asrtm asrtm;
	
	bool with_ops;
	OperatingPointList ops;
	
	
	bool with_awms;
	AwmList awms;
	std::set<Resources> awm_structure;
};






















#endif // ARGO_OP_PARSER_H
