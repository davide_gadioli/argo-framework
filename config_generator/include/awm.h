#ifndef ARGO_AWM_H
#define ARGO_AWM_H

#include <map>
#include <string>
#include <memory>
#include <list>

enum class Resources {
	CPU_QUOTA,
	MEM
};



extern std::map<std::string, Resources> resource_xml_name;
extern std::map<Resources, std::string> resource_metric_name;
extern std::map<Resources, std::string> resource_metric_type;




class Awm
{
public:
	Awm(std::string id):id(id) {}
	
	void AddResource( Resources resource, std::string value);
	
	std::map<Resources, std::string> constraints;
	std::string id;
};



typedef std::shared_ptr<Awm> AwmPtr;
typedef std::list<AwmPtr> AwmList;















#endif //ARGO_AWM_H