#ifndef ARGO_GOAL_H
#define ARGO_GOAL_H


#include <memory>
#include <string>
#include <list>


#include "monitor.h"



class Goal
{
public:
	Goal(MonitorPtr monitor, std::string name, std::string dFun, std::string cFun, std::string value);
	
	std::string GetDeclaration(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) {return name;}
	
private:
	
	std::string dec;
	std::string init;
	std::string name;
};


typedef std::shared_ptr<Goal> GoalPtr;
typedef std::list<GoalPtr> GoalList;






#endif //ARGO_GOAL_H