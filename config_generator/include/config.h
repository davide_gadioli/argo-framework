#ifndef ARGO_CONFIG_H
#define ARGO_CONFIG_H




// defined if it has the steady clock
#define WITH_STEADY_CLOCK

// defined if the compiler support nullptr
#define WITH_NULLPTR


// steady_clock fix hack
#ifndef WITH_STEADY_CLOCK
#define steady_clock monotonic_clock
#endif


// nullptr fix
#ifndef WITH_NULLPTR
#define nullptr NULL
#endif










#endif // ARGO_CONFIG_H
