#ifndef ARGO_MONITOR_H
#define ARGO_MONITOR_H


#include <list>
#include <string>
#include <memory>


class Monitor
{
public:	
	virtual std::string GetHeader(void) = 0;
	virtual std::string GetDeclaration(void) = 0;
	virtual std::string GetInitialization(void) = 0;
	
	virtual std::string GetName(void) = 0;
	virtual std::string GetElementType(void) = 0;
};


typedef std::shared_ptr<Monitor> MonitorPtr;
typedef std::list<MonitorPtr> MonitorList;




class ThroughputMonitor: public Monitor
{
public:
	ThroughputMonitor(std::string name, std::string number_element);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return "argo::monitor::Throughput_t"; }
	
private:
	std::string name;
	std::string number_element;
};


class MemoryMonitor: public Monitor
{
public:
	MemoryMonitor(std::string name, std::string number_element);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return "argo::monitor::Memory_t"; }
	
private:
	std::string name;
	std::string number_element;
};


class ProcessCPUMonitor: public Monitor
{
public:
	ProcessCPUMonitor(std::string name, std::string number_element);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return "argo::monitor::CpuUsage_t"; }
	
private:
	std::string name;
	std::string number_element;
};



class SystemCPUMonitor: public Monitor
{
public:
	SystemCPUMonitor(std::string name, std::string number_element);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return "argo::monitor::CpuUsage_t"; }
	
private:
	std::string name;
	std::string number_element;
};

class TimeMonitor: public Monitor
{
public:
	TimeMonitor(std::string name, std::string number_element);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return "argo::monitor::Time_t"; }
	
private:
	std::string name;
	std::string number_element;
};


class CustomMonitor: public Monitor
{
public:
	CustomMonitor(std::string name, std::string number_element, std::string type, std::string header, std::string params, std::string element_type);
	
	std::string GetDeclaration(void);
	std::string GetHeader(void);
	std::string GetInitialization(void);
	
	std::string GetName(void) { return name; }
	std::string GetElementType(void) { return element_type; }
	
private:
	std::string name;
	std::string number_element;
	std::string type;
	std::string header;
	std::string params;
	std::string element_type;
};







#endif //ARGO_MONITOR_H