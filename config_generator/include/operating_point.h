#ifndef ARGO_OPERATING_POINT_H
#define ARGO_OPERATING_POINT_H

#include <string>
#include <map>
#include <set>
#include <list>
#include <memory>



class OperatingPoint
{
public:
	
	OperatingPoint(void) {}
	~OperatingPoint();
	OperatingPoint( const OperatingPoint& op );
	
	void AddMetric( std::pair<std::string, std::string> metric );
	void AddParameter( std::pair<std::string, std::string> parameter );
	void AddState( std::string state );
	
	std::string GetFieldValue( std::string field_name );
	
	
	
	std::map<std::string, std::string> metrics;
	std::map<std::string, std::string> parameters;
	std::set<std::string> states;
};


typedef std::shared_ptr<OperatingPoint> OperatingPointPtr;
typedef std::list<OperatingPointPtr> OperatingPointList;






#endif //ARGO_OPERATING_POINT_H