#ifndef ARGO_ASRTM_H
#define ARGO_ASRTM_H


#include <string>
#include <memory>
#include <set>
#include <map>



class Asrtm
{
public:
	Asrtm(void);
	
	
	void AddStaticConstraint(std::string field_name, std::string cFun, std::string value);
	void AddDynamicConstraint( std::string goal_name, std::string field_name, std::string noise, std::string validty);
	void AddRankDefinition( std::string type, std::map< std::string, std::string > definition, std::string objective );
	void ChangeState( std::string new_state );
	
	
	
	
	std::string GetDeclaration(void);
	std::string GetInitialization(void);


	// the list of declared states
	std::set<std::string> state_list;
	
private:
	
	const std::string offset;
	std::string cmdList;
};


typedef std::shared_ptr<Asrtm> AsrtmPtr;














#endif //ARGO_ASRTM_H