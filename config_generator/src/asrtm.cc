#include "asrtm.h"


Asrtm::Asrtm(void):offset("\t\t")
{
	cmdList  = offset + "// Initializing the Application-Specific Run-Time Manager\n";
	cmdList += offset + "as_rtm = argo::asrtm::AsrtmPtr( new argo::asrtm::Asrtm( op_list ) );\n";
}



void Asrtm::ChangeState(std::string new_state)
{
	cmdList += "\n\n" + offset + "// changing the state, from here all the changes affects this state\n";
	cmdList += offset + "as_rtm->changeState(\"" + new_state + "\");\n";
	
	state_list.insert(new_state);
}

void Asrtm::AddDynamicConstraint(std::string goal_name, std::string field_name, std::string noise, std::string validty)
{
	cmdList += offset + "as_rtm->addDynamicConstraintOnBottom(" + goal_name + ", \"" + field_name + "\", " + noise + ", " + validty + ");\n";
}

void Asrtm::AddStaticConstraint(std::string field_name, std::string cFun, std::string value)
{
	cmdList += offset + "as_rtm->addStaticConstraintOnBottom(\"" + field_name + "\", argo::ComparisonFunction::" + cFun + ", " + value + ");\n";
}

std::string Asrtm::GetDeclaration(void)
{
	return "argo::asrtm::ArgoPtr as_rtm;\n";
}

std::string Asrtm::GetInitialization(void)
{
	return cmdList;
}

void Asrtm::AddRankDefinition(std::string type, std::map< std::string, std::string > definition, std::string objective)
{
	
	std::string structure = "{ ";
	for( std::map<std::string, std::string>::iterator field = definition.begin(); field != definition.end(); ++field )
	{
		structure += " { \"" + field->first + "\" , " + field->second + " }, ";
	}
	structure += " }";
	
	cmdList += offset + "as_rtm->set" + type + "Rank( " + structure + ", argo::RankObjective::" + objective + ");\n";
}

