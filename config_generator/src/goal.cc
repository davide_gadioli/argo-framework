#include "goal.h"


Goal::Goal(MonitorPtr monitor, std::string name, std::string dFun, std::string cFun, std::string value)
{
	dec = "std::shared_ptr<argo::monitor::Goal<" + monitor->GetElementType() + ">> " + name + ";\n";
	
	init = name + " = std::shared_ptr<argo::monitor::Goal<" + monitor->GetElementType() + ">>( new argo::monitor::Goal<" + monitor->GetElementType() + ">( " +
		monitor->GetName() + ", argo::DataFunction::" + dFun + ", argo::ComparisonFunction::" + cFun + ", " + value + ") );\n";
}


std::string Goal::GetDeclaration(void)
{
	return dec;
}

std::string Goal::GetInitialization(void)
{
	return init;
}
