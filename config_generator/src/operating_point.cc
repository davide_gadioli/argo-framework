#include "operating_point.h"

#include <stdexcept>


OperatingPoint::OperatingPoint(const OperatingPoint& op)
{
	this->metrics = op.metrics;
	this->parameters = op.parameters;
	this->states = op.states;
}
OperatingPoint::~OperatingPoint()
{
	metrics.clear();
	parameters.clear();
	states.clear();
}


void OperatingPoint::AddMetric(std::pair< std::string, std::string > metric)
{
	metrics.insert(metric);
}

void OperatingPoint::AddParameter(std::pair< std::string, std::string > parameter)
{
	parameters.insert(parameter);
}

void OperatingPoint::AddState(std::string state)
{
	states.insert(state);
}

std::string OperatingPoint::GetFieldValue(std::string field_name)
{
	
	// try with the metrics
	std::map<std::string, std::string>::iterator field = metrics.find(field_name);
	if ( field != metrics.end() )
		return field->second;
	
	
	// try with the params
	field = parameters.find(field_name);
	if ( field != parameters.end() )
		return field->second;
	
	
	// throw an exception
	throw std::logic_error("No field named \"" + field_name + "\" is found!");	
}


