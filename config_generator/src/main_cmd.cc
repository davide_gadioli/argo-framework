
// system include
#include <iostream>
#include <string>


// option parser
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>


// parser include
#include "parser.h"

/**
 * The decription of each testapp parameters
 */
namespace po = boost::program_options;
po::options_description opts_desc("Config generator Options");

/**
 * The map of all parameters values
 */
po::variables_map opts_vm;


void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
		po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);
	
	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}
}


using namespace std;





int main(int argc, char* argv [])
{
	// config var
	string op_list_path, config_path, op_post_path;
	string awm_list_path, out_src, out_hdr;
	
	
	// parse the arguments
	opts_desc.add_options()
	
		("help,h", "print this help message")
		
		("config,c", po::value<std::string>(&config_path)->
		default_value("no config"),
		 "The XML configuration of the Argo framework")
		
		("ops,o", po::value<std::string>(&op_list_path)->
		default_value("no op list"),
		 "The XML list of Operating Points")
		
		("awms,a", po::value<std::string>(&awm_list_path)->
		default_value("no awm list"),
		 "The XML list of Working Modes")
		
		("post_op,p", po::value<std::string>(&op_post_path)->
		default_value("no post op"),
		 "The XML that describe OPs post-processing")
		
		("out_src,s", po::value<std::string>(&out_src)->
		default_value("."),
		 "The output directory of source files")
		
		("out_hdr,d", po::value<std::string>(&out_hdr)->
		default_value("."),
		 "The output directory of the header file")
	;
	ParseCommandLine(argc, argv);
	
	
	// create the parser
	Parser parser;
	
	// parse the xml files
	if ( config_path.compare("no config") != 0)
	{
		parser.ParseConfiguration(config_path);
	}
	if ( op_list_path.compare("no op list") != 0 )
	{
		if (op_post_path.compare("no post op") != 0 )
		{
			parser.ParseOps(op_list_path, op_post_path);
		}
		else
		{
			parser.ParseOps(op_list_path);
		}
	}
	if ( awm_list_path.compare("no awm list") != 0 )
	{
		parser.ParseAwms(awm_list_path);
	}

	// create the output files
	parser.BuildFiles(out_hdr,out_src);

	return 0;
}
