#include <iostream>
#include <fstream>
#include <stdexcept>
#include <map>

#include "parser.h"


using namespace std;


// the xml parser
#define TIXML_USE_STL
#include "tinyxml2.h"
namespace xml = tinyxml2;


// --- helper functions for tinyxml2

// convert the return code to string
inline const string getXmlRetCode(const xml::XMLError ret_code)
{
	if ( ret_code == xml::XMLError::XML_NO_ATTRIBUTE )
		return "XML_NO_ATTRIBUTE";
	
	if ( ret_code == xml::XMLError::XML_WRONG_ATTRIBUTE_TYPE )
		return "XML_WRONG_ATTRIBUTE_TYPE";
	
	if ( ret_code == xml::XMLError::XML_ERROR_FILE_NOT_FOUND )
		return "XML_ERROR_FILE_NOT_FOUND";
	
	if ( ret_code == xml::XMLError::XML_ERROR_FILE_COULD_NOT_BE_OPENED )
		return "XML_ERROR_FILE_COULD_NOT_BE_OPENED";
	
	if ( ret_code == xml::XMLError::XML_ERROR_FILE_READ_ERROR )
		return "XML_ERROR_FILE_READ_ERROR";
	
	if ( ret_code == xml::XMLError::XML_ERROR_ELEMENT_MISMATCH )
		return "XML_ERROR_ELEMENT_MISMATCH";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_ELEMENT )
		return "XML_ERROR_PARSING_ELEMENT";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_ATTRIBUTE )
		return "XML_ERROR_PARSING_ATTRIBUTE";
	
	if ( ret_code == xml::XMLError::XML_ERROR_IDENTIFYING_TAG )
		return "XML_ERROR_IDENTIFYING_TAG";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_TEXT )
		return "XML_ERROR_PARSING_TEXT";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_CDATA )
		return "XML_ERROR_PARSING_CDATA";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_COMMENT )
		return "XML_ERROR_PARSING_COMMENT";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_DECLARATION )
		return "XML_ERROR_PARSING_DECLARATION";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING_UNKNOWN )
		return "XML_ERROR_PARSING_UNKNOWN";
	
	if ( ret_code == xml::XMLError::XML_ERROR_EMPTY_DOCUMENT )
		return "XML_ERROR_EMPTY_DOCUMENT";
	
	if ( ret_code == xml::XMLError::XML_ERROR_MISMATCHED_ELEMENT )
		return "XML_ERROR_MISMATCHED_ELEMENT";
	
	if ( ret_code == xml::XMLError::XML_ERROR_PARSING )
		return "XML_ERROR_PARSING";
	
	if ( ret_code == xml::XMLError::XML_CAN_NOT_CONVERT_TEXT )
		return "XML_CAN_NOT_CONVERT_TEXT";
	
	if ( ret_code == xml::XMLError::XML_NO_TEXT_NODE )
		return "XML_NO_TEXT_NODE";
	
	if ( ret_code == xml::XMLError::XML_SUCCESS )
		return "XML_SUCCESS";
	
	if ( ret_code == xml::XMLError::XML_NO_ERROR )
		return "XML_NO_ERROR";
	
	return "UNKNOWN_ERROR";
}

// throw an exception if it fails
inline const void xmlCheck(xml::XMLError error)
{
	if ((error != xml::XMLError::XML_NO_ERROR) && (error != xml::XMLError::XML_SUCCESS))
		throw runtime_error(getXmlRetCode(error));
}


// recursive function in order to find the resource elements in the awm
void findResourcesValues(xml::XMLElement* root, std::list<xml::XMLElement*>& foundElements )
{
	// recursive exit
	if ( root == nullptr )
		return;
	
	// check if the tag element is a resource descriptor
	string xml_tag_name( root->Name() );
	std::map<std::string, Resources>::iterator element = resource_xml_name.find(xml_tag_name);
	if ( element != resource_xml_name.end() )
		foundElements.push_back(root);
	
	// go deep
	findResourcesValues(root->FirstChildElement(), foundElements);
	
	// go next
	findResourcesValues(root->NextSiblingElement(), foundElements);
}




Parser::Parser() {
	with_awms = false;
	with_ops = false;
	with_config = false;
}




void Parser::ParseConfiguration(string xml_file)
{
	// --- take the root element
	with_config = true;
	xml::XMLDocument xml_configuration;
	xml::XMLError ret = xml_configuration.LoadFile(xml_file.c_str());
	xmlCheck(ret);
	xml::XMLElement* configuration = xml_configuration.FirstChildElement();
	
	
	// --- loop over the sections
	for( xml::XMLElement* section = configuration->FirstChildElement(); section != nullptr; section = section->NextSiblingElement() )
	{
		
		// get the name
		string section_name = std::string(section->Name());
		cout << "Parsing " << section_name << endl;
		

		// check if they are the monitors
		if ( section_name.compare("monitors") == 0)
		{
			// loop over the monitors
			for( xml::XMLElement* xml_monitor = section->FirstChildElement(); xml_monitor != nullptr; xml_monitor = xml_monitor->NextSiblingElement() )
			{
				// get the name
				string monitor_name( xml_monitor->Attribute("name") );
				
				// get the type
				string monitor_type( xml_monitor->Attribute("type") );
				
				// get the header
				string monitor_nelem( xml_monitor->Attribute("elements_number") );
				
				// construct the monitor
				MonitorPtr parsed_monitor;
				if ( monitor_type.compare("Throughput") == 0)
					parsed_monitor = MonitorPtr( new ThroughputMonitor(monitor_name, monitor_nelem) );
				if ( monitor_type.compare("Memory") == 0)
					parsed_monitor = MonitorPtr( new MemoryMonitor(monitor_name, monitor_nelem) );
				if ( monitor_type.compare("ProcessCPU") == 0)
					parsed_monitor = MonitorPtr( new ProcessCPUMonitor(monitor_name, monitor_nelem) );
				if ( monitor_type.compare("SystemCPU") == 0)
					parsed_monitor = MonitorPtr( new SystemCPUMonitor(monitor_name, monitor_nelem) );
				if ( monitor_type.compare("Time") == 0)
					parsed_monitor = MonitorPtr( new TimeMonitor(monitor_name, monitor_nelem) );
				if ( monitor_type.compare("Custom") == 0)
				{
					// go to the specific node
					xml::XMLElement* xml_spec = xml_monitor->FirstChildElement();
					
					// get the header path
					string monitor_header( xml_spec->Attribute("header_path") );
					
					// get the additional parameters
					string monitor_params( xml_spec->Attribute("additional_params") );
					
					// get the class name
					string monitor_class( xml_spec->Attribute("class_name") );
					
					// get the type of the monitor
					string monitor_element_type( xml_spec->Attribute("element_type") );
					
					
					parsed_monitor = MonitorPtr( new CustomMonitor(monitor_name, monitor_nelem, monitor_class, monitor_header, monitor_params, monitor_element_type) );
				}
				
				// Check Monitor Type
				if (!parsed_monitor)
					throw std::runtime_error("Monitor type unknown");
				
				
				// save the monitor
				monitor_list.push_back(parsed_monitor);
			}
			
			// continue to the next section
			continue;
		}
		
		
		// check if they are the goals
		if ( section_name.compare("goals") == 0)
		{
			// loop over the goals
			for( xml::XMLElement* xml_goal = section->FirstChildElement(); xml_goal != nullptr; xml_goal = xml_goal->NextSiblingElement() )
			{
				// get the data function
				string goal_dFun( xml_goal->Attribute("dFun") );
				
				// get the comparison function
				string goal_cFun( xml_goal->Attribute("cFun") );
				
				// get the name of the goal
				string goal_name( xml_goal->Attribute("name") );
				
				// get the value of the goal
				string goal_value( xml_goal->Attribute("value") );
				
				// get the name of the monitor
				string goal_monitor_name( xml_goal->Attribute("monitor") );
				
				// get the reference of the monitor
				MonitorPtr monitor;
				for( MonitorList::iterator it = monitor_list.begin(); it != monitor_list.end(); ++it)
				{
					if ((*it)->GetName().compare(goal_monitor_name) == 0)
					{
						monitor = *it;
						break;
					}
				}
				
				// add the goal to the goal list
				GoalPtr goal = GoalPtr( new Goal( monitor, goal_name, goal_dFun, goal_cFun, goal_value) );
				goal_list.push_back( goal );
			}
			
			// continue to the next section
			continue;
		}
		
		
		// check if they are the goals
		if ( section_name.compare("states") == 0)
		{
			
			// get the starting state name
			string state_start( section->Attribute("start") );
			
			// loop over the states
			for( xml::XMLElement* xml_state = section->FirstChildElement(); xml_state != nullptr; xml_state = xml_state->NextSiblingElement() )
			{
				// go to the state described
				string state_name( xml_state->Name() );
				asrtm.ChangeState(state_name);
				
				
				// loop over the subsections
				for( xml::XMLElement* subsection = xml_state->FirstChildElement(); subsection != nullptr; subsection = subsection->NextSiblingElement() )
				{
					// get the subsection name
					string subsection_name( subsection->Name() );					
					
					// check if it's the constraints declaration
					if ( subsection_name.compare("constraints") == 0 )
					{
						// loop over the constraints
						for( xml::XMLElement* xml_constraint = subsection->FirstChildElement(); xml_constraint != nullptr; xml_constraint = xml_constraint->NextSiblingElement() )
						{
							// get the constriant type
							string constraint_type( xml_constraint->Name() );
							
							
							// check if static or dynamic
							if ( constraint_type.compare("dynamic") == 0 )
							{
								// get the fields
								string dynamic_goal( xml_constraint->Attribute("goal") );
								string dynamic_field_name( xml_constraint->Attribute("field_name") );
								string dynamic_noise( xml_constraint->Attribute("noise") );
								string dynamic_validity( xml_constraint->Attribute("min_elements") );
								
								// add the constraint
								asrtm.AddDynamicConstraint(dynamic_goal, dynamic_field_name, dynamic_noise, dynamic_validity);
							} 
							else
							{
								// get the fields
								string static_field_name( xml_constraint->Attribute("field_name") );
								string static_cFun( xml_constraint->Attribute("cFun") );
								string static_value( xml_constraint->Attribute("value") );
								
								// add the constraint
								asrtm.AddStaticConstraint(static_field_name, static_cFun, static_value);
							}
							
						}
					}
					
					
					// check if it's the rank declaration
					if ( subsection_name.compare("rank") == 0)
					{
						// get the type and objective
						string rank_type( subsection->Attribute("type") );
						string rank_objective( subsection->Attribute("objective") );
						
						// initialize the structure
						map<string, string> structure;
						
						
						// loop over the rank metrics
						for( xml::XMLElement* xml_metric = subsection->FirstChildElement(); xml_metric != nullptr; xml_metric = xml_metric->NextSiblingElement() )
						{
							// get the name of the field
							string metric_name( xml_metric->Name() );
							
							// get the coufficient
							string metric_coef( xml_metric->GetText() );
							
							// insert in the map
							structure.insert(pair<string, string>(metric_name, metric_coef));
						}
						
						// set the rank
						asrtm.AddRankDefinition(rank_type, structure, rank_objective);
					}
				}
			}
			// go to the starting state
			asrtm.ChangeState(state_start);
		}
	}
	
	// --- close the document
	xml_configuration.Clear();
}



void Parser::ParseOps(string xml_file)
{
	// --- take the root element
	with_ops = true;
	xml::XMLDocument xml_op_list;
	xml::XMLError ret = xml_op_list.LoadFile(xml_file.c_str());
	xmlCheck(ret);
	xml::XMLElement* op_list = xml_op_list.FirstChildElement();
	
	cout << "Parsing Operting Points list" << endl;
	
	
	// loop over all the ops
	for( xml::XMLElement* xml_op = op_list->FirstChildElement(); xml_op != nullptr; xml_op = xml_op->NextSiblingElement() )
	{
		// create the Operating Point list
		OperatingPointPtr op( new OperatingPoint() );
		
		// loop over the ops sections
		for( xml::XMLElement* op_section = xml_op->FirstChildElement(); op_section != nullptr; op_section = op_section->NextSiblingElement() )
		{
			// get the section name
			string section_name( op_section->Name() );
			
			// check if they are parameters
			if ( section_name.compare("parameters") == 0 )
			{
				// loop over the elements
				for( xml::XMLElement* xml_element = op_section->FirstChildElement(); xml_element != nullptr; xml_element = xml_element->NextSiblingElement() )
				{
					// the the name and value
					string name( xml_element->Attribute("name") );
					string value( xml_element->Attribute("value") );
					
					// add the parameter
					op->AddParameter( {name, value} );
				}
			}
			
			// check if they are metrics
			if ( section_name.compare("system_metrics") == 0 )
			{
				// loop over the elements
				for( xml::XMLElement* xml_element = op_section->FirstChildElement(); xml_element != nullptr; xml_element = xml_element->NextSiblingElement() )
				{
					// the the name and value
					string name( xml_element->Attribute("name") );
					string value( xml_element->Attribute("value") );
					
					// add the parameter
					op->AddMetric( {name, value} );
				}
			}
			
			
			// check if they are states
			if ( section_name.compare("states") == 0 )
			{
				// loop over the elements
				for( xml::XMLElement* xml_element = op_section->FirstChildElement(); xml_element != nullptr; xml_element = xml_element->NextSiblingElement() )
				{
					// the the name and value
					string name( xml_element->Name() );
					
					// add the parameter
					op->AddState( name );
				}
			}
			
			
		}
		
		// check if they have at least one state
		if ( op->states.empty() )
		{
			op->states.insert("default");
		}
		
		// add the OP to the list
		ops.push_back(op);
	}
	
	// --- close the xml document
	xml_op_list.Clear();
}

void Parser::ParseAwms(string xml_file)
{
	// --- take the root element
	with_awms = true;
	xml::XMLDocument xml_awm_list;
	xml::XMLError ret = xml_awm_list.LoadFile(xml_file.c_str());
	xmlCheck(ret);
	xml::XMLElement* awm_list = xml_awm_list.FirstChildElement();
	
	cout << "Parsing Working Mode list" << endl;
	
	// go deep inside until the actual awm list is found
	while( std::string(awm_list->Name()).compare("awms") != 0)
	{
		awm_list = awm_list->FirstChildElement();
	}
	
	
	// loop over the awms
	for( xml::XMLElement* xml_awm = awm_list->FirstChildElement(); xml_awm != nullptr; xml_awm = xml_awm->NextSiblingElement() )
	{
		// get the id of the awm
		string awm_id( xml_awm->Attribute("id") );
		
		// create the awm
		AwmPtr awm = AwmPtr( new Awm(awm_id) );
		
		
		// get the list of resources
		std::list<xml::XMLElement*> resElements;
		findResourcesValues(xml_awm, resElements);
		
		// loop over the resources
		for( std::list<xml::XMLElement*>::iterator res = resElements.begin(); res != resElements.end(); ++res )
		{
			// Get the resource id and add to the set of the available
			Resources resource_name = resource_xml_name.at( (*res)->Name() );
			awm_structure.insert(resource_name);
			
			// Get the resource qty
			string resource_quantity( (*res)->Attribute("qty") );
			
			// Add it to the awm
			awm->AddResource(resource_name, resource_quantity);
		}
		
		// insert the awm
		awms.push_back(awm);
	}
	
	// --- close the xml document
	xml_awm_list.Clear();

}


void Parser::ParseOps(string xml_file, string post_file)
{ 
	// Parse the Ops
	ParseOps(xml_file);
	
	
	// Perform the post-processing about the Operating Points
	xml::XMLDocument xml_op_post;
	xml::XMLError ret = xml_op_post.LoadFile(post_file.c_str());
	xmlCheck(ret);
	xml::XMLElement* xml_states = xml_op_post.FirstChildElement();
	
	cout << "Post-processing the Operting Points list" << endl;
	
	// Get the states
	xml_states = xml_states->FirstChildElement();
	
	string keep_state( xml_states->Attribute("keep_previous_state") );
	
	// check if we need to remove the op states
	if ( keep_state.compare("false") == 0 )
	{
		for( OperatingPointList::iterator op = ops.begin(); op != ops.end(); ++op)
		{
			(*op)->states.clear();	
		}
	}
	
	// loop over the states
	for( xml::XMLElement* xml_state = xml_states->FirstChildElement(); xml_state != nullptr; xml_state = xml_state->NextSiblingElement() )
	{
		// Get the state name
		string state_name( xml_state->Name() );
		
		// loop over the op_list
		for( OperatingPointList::iterator op = ops.begin(); op != ops.end(); ++op)
		{
			// Tell if the op belong to the state
			bool belong = true;
			
			// Loop over the field filter
			for( xml::XMLElement* xml_filter = xml_state->FirstChildElement(); xml_filter != nullptr; xml_filter = xml_filter->NextSiblingElement() )
			{
				
				// Get the type of the filter
				string state_filter( xml_filter->Name() );
				
				// Check "all" filter special filter
				if ( state_filter.compare("all") == 0 )
				{
					continue;
				}
				
				
				// Otherwise get the field name and value
				string filter_field_name( xml_filter->Attribute("field_name") );
				string filter_value( xml_filter->Attribute("value") );
				
				
				// Get the value of the field of the operating point
				string op_value = (*op)->GetFieldValue(filter_field_name);
				
				// Get the two value
				float op_valuef = std::stod(op_value);
				float filter_valuef = std::stod(filter_value);
				
				
				// Check if the op belong to it
				if ( state_filter.compare("gt") == 0 )
				{
					belong = belong && ( op_valuef > filter_valuef);
				}
				if ( state_filter.compare("ge") == 0 )
				{
					belong = belong && ( op_valuef >= filter_valuef);
				}
				if ( state_filter.compare("lt") == 0 )
				{
					belong = belong && ( op_valuef < filter_valuef);
				}
				if ( state_filter.compare("le") == 0 )
				{
					belong = belong && ( op_valuef <= filter_valuef);
				}
				if ( state_filter.compare("eq") == 0 )
				{
					belong = belong && ( op_valuef == filter_valuef);
				}
				
			}
			
			// add the state
			if (belong)
			{
				(*op)->AddState( state_name );
			}
		}
	}
	
	
	// close the xml document
	xml_op_post.Clear();
}









void Parser::BuildFiles(string out_header, string out_src)
{
	/*
	 * Create the argo header
	 */
	
	
	// --- open the file
	ofstream argo;
	argo.open(out_header + "/argo.h", ios::out | ios::trunc);
	
	// --- write the header
	argo << "#ifndef ARGO_ARGO_H" << endl;
	argo << "#define ARGO_ARGO_H" << endl;
	argo << endl << endl;
	argo << "#include \"argo/asrtm/asrtm.h\"" << endl;
	argo << "#include \"argo/monitor/monitor.h\"" << endl;
	for(MonitorList::iterator monitor = monitor_list.begin(); monitor != monitor_list.end(); ++monitor)
	{
		argo << (*monitor)->GetHeader();
	} 
	argo << endl << endl;
	argo << "// WARNING: autogenerated file" << endl;
	argo << endl << endl;
	argo << "namespace argo {" << endl;
	argo << endl << endl;
	
	
	// --- write the awm stuff
	if (with_awms) {
		argo << "\t// Barbeque awm_constraint structure" << endl;
		argo << "\ttypedef struct bbque_resource {" << endl;
		for(std::set<Resources>::iterator res = awm_structure.begin(); res != awm_structure.end(); ++res)
		{
			argo << "\t\targo::asrtm::OPMetric_t " << resource_metric_name.at(*res) << ";" << endl;
		}
		argo << "\t} bbque_resource_t;" << endl;
		argo << endl;
		argo << "\t// Awm constraints vector" << endl;
		argo << "\textern const std::vector<bbque_resource_t> res_constraints;" << endl;
		argo << endl << endl;
	}
	
	// --- write the op_list init
	if (with_ops)
	{
		argo << "\t// Operating Point list" << endl;
		argo << "\textern const argo::asrtm::OperatingPointsList op_list;" << endl;
		argo << endl << endl;
	}
	
	// --- write the asrtm stuff
	if (with_config) {
		
		
		// if no Operating Points are provided, the asrtm can't be created
		if (with_ops) {
			argo << "\t// The Application-Specific Run-Time Manager" << endl;
			argo << "\textern argo::asrtm::AsrtmPtr as_rtm;" << endl;
			argo << endl;
		}
		argo << "\t// Declaring the application-specific monitors" << endl;
		for(MonitorList::iterator monitor = monitor_list.begin(); monitor != monitor_list.end(); ++monitor)
		{
			argo << "\textern " << (*monitor)->GetDeclaration();
		}
		argo << endl;
		argo << "\t// Declaring the application-specific goals" << endl;
		for(GoalList::iterator goal = goal_list.begin(); goal != goal_list.end(); ++goal)
		{
			argo << "\textern " << (*goal)->GetDeclaration();
		}
		argo << endl << endl;
		argo << "\t// Helper function that define the framework behaviour" << endl;
		argo << "\tvoid SetupArgo( void );" << endl;
		argo << endl << endl;
	}	
	
	// --- write the footer
	argo << "} // namespace argo" << endl;
	argo << endl;
	argo << "#endif // ARGO_ARGO_H" << endl;
	argo.close();
	
	
	
	
	
	/*
	 * Create the op_list src file
	 */
	if ((with_ops) || (with_awms))
	{
		// --- open the file
		ofstream op_list;
		op_list.open(out_src + "/op_list.cc", ios::out | ios::trunc);
		
		
		
		// --- write the header
		op_list << "#include \"argo.h\"" << endl;
		op_list << endl;
		op_list << "// WARNING! Autogenerated file!" << endl;
		op_list << endl;
		op_list << "namespace argo {" << endl;
		op_list << endl << endl;
		
		
		
		// --- write the awm list
		if (with_awms)
		{
			op_list << "\t// Barbeque awm constraint values" << endl;
			op_list << "\tconst std::vector<bbque_resource_t> res_constraints = {" << endl;
			for( AwmList::iterator awm = awms.begin(); awm != awms.end(); ++awm)
			{
				// write the first part
				op_list << "\t\t{  ";
				
				for( std::set<Resources>::iterator res = awm_structure.begin(); res != awm_structure.end(); ++res)
				{
					op_list << "/* " << resource_metric_name.at(*res) << " */ " << (*awm)->constraints.at(*res) << ", ";
				}
				op_list << "}, " << endl;
			}
			op_list << "\t};" << endl;
			op_list << endl << endl;
		}
		
		
		// --- write the op list
		if (with_ops)
		{
			op_list << "\t// The Operating Points list" << endl;
			op_list << "\tconst argo::asrtm::OperatingPointsList op_list = {" << endl;
			unsigned int id = 0;
			for( OperatingPointList::iterator op = ops.begin(); op != ops.end(); ++op, id++ )
			{
				op_list << "\t\t{ // Operating Point " << id << endl;
				op_list << "\t\t\t{  // Parameters" << endl;
				for( std::map<std::string, std::string>::iterator param = (*op)->parameters.begin(); param != (*op)->parameters.end(); ++param)
				{
					op_list << "\t\t\t\t{ \"" + param->first + "\", " + param->second + " }," << endl;
				}
				op_list << "\t\t\t}," << endl;
				op_list << "\t\t\t{ // Metrics" << endl;
				for( std::map<std::string, std::string>::iterator metric = (*op)->metrics.begin(); metric != (*op)->metrics.end(); ++metric)
				{
					op_list << "\t\t\t\t{ \"" + metric->first + "\", " + metric->second + " }," << endl;
				}
				op_list << "\t\t\t}," << endl;
				op_list << "\t\t\t{ // States" << endl;
				for( std::set<std::string>::iterator state = (*op)->states.begin(); state != (*op)->states.end(); ++state)
				{
					op_list << "\t\t\t\t{ \"" << (*state) << "\" }," << endl;
				}
				op_list << "\t\t\t}" << endl;
				op_list << "\t\t}," << endl;
			}
			op_list << "\t};" << endl;
			op_list << endl << endl;
		}
		
		// --- write the footer
		op_list << "} // namespace argo" << endl;
		op_list.close();
	}
	
	
	/*
	 * Create the argo src file
	 */
	if (with_config)
	{
		argo.open(out_src + "/argo.cc", ios::out | ios::trunc);
	
	
		// --- write the header
		argo << "#include \"argo.h\"" << endl;
		argo << endl;
		argo << "// WARNING! Autogenerated file!" << endl;
		argo << endl;
		argo << "namespace argo {" << endl;
		argo << endl << endl;
	
	
		// --- declare the asrtm
		if (with_ops)
		{
			argo << "\t// The Applicatio-Specific Run-Time Manager" << endl;
			argo << "\targo::asrtm::AsrtmPtr as_rtm;" << endl;
			argo << endl;
		}
	
	
		// --- write the monitor/goal declaration
		argo << "\t// Monitors declaration" << endl;
		for(MonitorList::iterator monitor = monitor_list.begin(); monitor != monitor_list.end(); ++monitor)
		{
			argo << "\t" << (*monitor)->GetDeclaration();
		}
		argo << endl;
		argo << "\t// Goals declaration" << endl;
		for(GoalList::iterator goal = goal_list.begin(); goal != goal_list.end(); ++goal)
		{
			argo << "\t" << (*goal)->GetDeclaration();
		}
		argo << endl << endl;
	
		// --- write the setup functions
		argo << "\t// Implementation of the Argo behaviour" << endl;
		argo << "\tvoid SetupArgo( void )" << endl;
		argo << "\t{" << endl;
	
		// write the monitor initialization
		argo << "\t\t// Monitors initialization" << endl;
		for(MonitorList::iterator monitor = monitor_list.begin(); monitor != monitor_list.end(); ++monitor)
		{
			argo << "\t\t" << (*monitor)->GetInitialization();
		}
		argo << endl << endl;
	
		// write the goal initialization
		argo << "\t\t// Goal initialization" << endl;
		for(GoalList::iterator goal = goal_list.begin(); goal != goal_list.end(); ++goal)
		{
			argo << "\t\t" << (*goal)->GetInitialization();
		}
		argo << endl << endl;
	
		// write the asrtm setup
		if (with_ops)
		{
			argo << asrtm.GetInitialization();
		}
		argo << "\t}" << endl;
		argo << endl << endl;
	
	
	
		// --- write the footer
		argo << "} // namespace argo" << endl;
		argo.close();
	}
}

