#include "awm.h"


// usefull mapped values
std::map<std::string, Resources> resource_xml_name =
{
	{ "pe", Resources::CPU_QUOTA },
	{ "mem", Resources::MEM      }
};

std::map<Resources, std::string> resource_metric_name =
{
	{ Resources::CPU_QUOTA, "cpu_usage" },
	{ Resources::MEM      , "memory"}
};

std::map<Resources, std::string> resource_metric_type =
{
	{ Resources::CPU_QUOTA, "argo::CpuUsage_t" },
	{ Resources::MEM      , "argo::Memory_t"}
};


void Awm::AddResource(Resources resource, std::string value)
{
	constraints.insert(std::pair<Resources, std::string>(resource, value));

}
