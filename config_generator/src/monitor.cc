#include <monitor.h>


ThroughputMonitor::ThroughputMonitor(std::string name, std::string number_element)
{
	this->name = name;
	this->number_element = number_element;
}
std::string ThroughputMonitor::GetDeclaration(void)
{
	return "argo::monitor::ThroughputMonitorPtr " + name + ";\n";
}
std::string ThroughputMonitor::GetHeader(void)
{
	return "";
}
std::string ThroughputMonitor::GetInitialization(void)
{
	return name + " = argo::monitor::ThroughputMonitorPtr( new argo::monitor::ThroughputMonitor(" + number_element + ") );\n";
}



MemoryMonitor::MemoryMonitor(std::string name, std::string number_element)
{
	this->name = name;
	this->number_element = number_element;
}
std::string MemoryMonitor::GetDeclaration(void)
{
	return "argo::monitor::MemoryMonitorPtr " + name + ";\n";
}
std::string MemoryMonitor::GetHeader(void)
{
	return "";
}
std::string MemoryMonitor::GetInitialization(void)
{
	return name + " = argo::monitor::MemoryMonitorPtr( new argo::monitor::MemoryMonitor(" + number_element + ") );\n";
}



ProcessCPUMonitor::ProcessCPUMonitor(std::string name, std::string number_element)
{
	this->name = name;
	this->number_element = number_element;
}
std::string ProcessCPUMonitor::GetDeclaration(void)
{
	return "argo::monitor::ProcessCpuUsageMonitorPtr " + name + ";\n";
}
std::string ProcessCPUMonitor::GetHeader(void)
{
	return "";
}
std::string ProcessCPUMonitor::GetInitialization(void)
{
	return name + " = argo::monitor::ProcessCpuUsageMonitorPtr( new argo::monitor::ProcessCpuUsageMonitor(" + number_element + ") );\n";
}




SystemCPUMonitor::SystemCPUMonitor(std::string name, std::string number_element)
{
	this->name = name;
	this->number_element = number_element;
}
std::string SystemCPUMonitor::GetDeclaration(void)
{
	return "argo::monitor::SystemCpuUsageMonitorPtr " + name + ";\n";
}
std::string SystemCPUMonitor::GetHeader(void)
{
	return "";
}
std::string SystemCPUMonitor::GetInitialization(void)
{
	return name + " = argo::monitor::SystemCpuUsageMonitorPtr( new argo::monitor::SystemCpuUsageMonitor(" + number_element + ") );\n";
}



TimeMonitor::TimeMonitor(std::string name, std::string number_element)
{
	this->name = name;
	this->number_element = number_element;
}
std::string TimeMonitor::GetDeclaration(void)
{
	return "argo::monitor::TimeMonitorPtr " + name + ";\n";
}
std::string TimeMonitor::GetHeader(void)
{
	return "";
}
std::string TimeMonitor::GetInitialization(void)
{
	return name + " = argo::monitor::TimeMonitorPtr( new argo::monitor::TimeMonitor(" + number_element + ") );\n";
}


CustomMonitor::CustomMonitor(std::string name, std::string number_element, std::string type, std::string header, std::string params, std::string element_type)
{
	this->name = name;
	this->number_element = number_element;
	this->type = type;
	this->header = header;
	this->element_type = element_type;
	this->params = params;
}
std::string CustomMonitor::GetDeclaration(void)
{
	return "std::shared_ptr< " + type + " > " + name + ";\n";
}
std::string CustomMonitor::GetHeader(void)
{
	if (header.empty())
		return "";
	
	return "#include \"" + header + "\" // for " + type + " monitor\n";
}
std::string CustomMonitor::GetInitialization(void)
{
    if (params.compare("") == 0)
        return name + " = std::shared_ptr<" + type + ">( new " + type + "(" + number_element + ") );\n";
    else
        return name + " = std::shared_ptr<" + type + ">( new " + type + "(" + number_element + ", " + params + ") );\n";
}











