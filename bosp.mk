ifdef CONFIG_EXTERNAL_ARGO

# Targets provided by this project
.PHONY: argo clean_argo

# Add this to the "external" target
external: argo
clean_external: clean_argo

MODULE_EXTERNAL_ARGO = external/optional/argo

ARGO_OPTIONS += -DLIB_STATIC=OFF
ARGO_OPTIONS += -DLIB_DYNAMIC=ON


argo:
	@echo
	@echo "==== Building Argo Library ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_EXTERNAL_ARGO)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_EXTERNAL_ARGO)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_EXTERNAL_ARGO)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) $(ARGO_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_EXTERNAL_ARGO)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_argo:
	@echo
	@echo "==== Clean-up Argo Library ($(BUILD_TYPE)) ===="
	@rm -rf $(BUILD_DIR)/lib/libargo* $(BUILD_DIR)/include/argo
	@rm -rf $(MODULE_EXTERNAL_ARGO)/build
	@echo

else # CONFIG_EXTERNAL_ARGO

argo:
	$(warning external/argo module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_ARGO
