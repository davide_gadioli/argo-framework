/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_LOG_H
#define ARGO_LOG_H



#define LOG_ENABLED 0


#if LOG_ENABLED > 0
#define LOG_NEW_LINE(what) Log::logNewLine(what,__PRETTY_FUNCTION__);
 #define ADD_INDENT() Log::addIndent();
 #define REMOVE_INDENT() Log::removeIndent();
 #define NEW_LINE() Log::newLine();
 #define LOG(what) Log::log(what, __PRETTY_FUNCTION__);
 #define INDENT() Log::indent();
#else
 #define LOG_NEW_LINE(what)
 #define ADD_INDENT()
 #define REMOVE_INDENT()
 #define NEW_LINE()
 #define LOG(what)
 #define INDENT()
#endif


#include <cstdint>
#include <string>


namespace argo {


class Log {
public:
    static const uint8_t whiteSpaceTick = 4;

    static uint8_t indentLevel;

 	static void addIndent();

 	static void removeIndent();

	static void logNewLine(std::string what, const std::string function_name);

	static void log(std::string what, const std::string function_name);

 	static void indent();

 	static void newLine();

};


}

#endif // ARGO_LOG_H
