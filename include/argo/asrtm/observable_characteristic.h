/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ASRTM_OBSERVABLE_CHARACTERISTIC_H
#define ASRTM_OBSERVABLE_CHARACTERISTIC_H

#include <memory>
#include <vector>
#include <list>
#include <functional>
#include <algorithm>
#include <string>

#include "argo/asrtm/constraint.h"
#include "argo/monitor/goal.h"
#include "argo/asrtm/model.h"
#include "argo/asrtm/operating_point.h"
#include "argo/asrtm/log.h"
#include "argo/config.h"



/** Optimize branch prediction for "untaken" */
#define unlikely(x)     __builtin_expect((x),0)


/** Logging macro (used only inside the metric) **/
#if LOG_ENABLED > 0
 #include <string>
 #include <iostream>
 #include <algorithm>
 #define PRINT_CHARACTERISTIC() do {\
   LOG_NEW_LINE("Constraint on '" + metricName +  "', with observation window") \
   std::string comparisonFun;\
   if (cFun == ComparisonFunction::Greater) {\
    comparisonFun = ">";\
   }\
   if (cFun == ComparisonFunction::GreaterOrEqual) {\
    comparisonFun = ">=";\
   }\
   if (cFun == ComparisonFunction::Less) {\
    comparisonFun = "<";\
   }\
   if (cFun == ComparisonFunction::LessOrEqual) {\
    comparisonFun = "<=";\
   }\
   LOG_NEW_LINE("OP valid if its value is " + comparisonFun + " " + std::to_string(goalValue))\
   ADD_INDENT()\
   int idZeroPaddingNumber =  std::to_string(sortedOPs.size() - 1).size();\
   int valueZeroPaddingNumber = 0;\
   for( auto it = opValues.begin(); it != opValues.end(); ++it) {\
    valueZeroPaddingNumber = std::max(valueZeroPaddingNumber, (int) std::to_string((*it)*coefError).size());\
   }\
   int zeroPadding = std::max(idZeroPaddingNumber, valueZeroPaddingNumber);\
   INDENT()\
   LOG("OPs id: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - ((int) std::to_string(*it).size())); i++) {\
      LOG(" ")\
    }\
    LOG(std::to_string(*it) + " ")\
   }\
   NEW_LINE()\
   INDENT()\
   LOG("values: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - ((int) std::to_string(opValues[*it]*coefError).size())); i++) {\
      LOG(" ")\
    }\
    LOG(std::to_string(opValues[*it]*coefError) + " ")\
   }\
   NEW_LINE()\
   INDENT()\
   LOG(" limit: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - 1); i++) {\
      LOG(" ")\
    }\
    if(*it == *limitOP) {\
      LOG("* ")\
    } else {\
      LOG("  ")\
    }\
   }\
   NEW_LINE()\
   REMOVE_INDENT()\
   } while(0);                                                                    
#else
 #define PRINT_CHARACTERISTIC()
#endif





namespace argo { namespace asrtm {
	
	
  /**
   * @details This class represents a characteristic with
   *          an observation window handled by a monitor.
   *          If it's used as a constraint it must have
   *          a goal.
   * 
   * @brief This class represent a dynamic constraint
   */
  template <typename dataType>
  class ObservableCharacteristic: public Constraint {
  public:
    
    typedef argo::monitor::Goal<dataType> Goal;
    typedef std::shared_ptr<Goal> GoalPtr;
    
    typedef std::function<bool(dataType, dataType)> ComparisonFunctor;
    typedef std::function<bool(ObservableCharacteristic<dataType>*, OperatingPointID_t, OperatingPointID_t)> SortingFunctor;
    
    
    
    
    /**
     * @brief Constructor of an observable characteristic
     * 
     * @param window The observation window used to get new data
     * 
     * @param opList The list of operating point obtained at design time
     * 
     * @param goalID The id of the goal associated to this observable characteristic
     * 
     * @param metricName The name of the metric in the op list
     * 
     */    
    ObservableCharacteristic(GoalPtr goal, OperatingPointsList& opList, std::string metricName, bool isRelaxable = false);
    ObservableCharacteristic(const ObservableCharacteristic& characteristic);
    ~ObservableCharacteristic();
    

    /**
      * @brief Set the observation tolerance
      *
      * @param percentage the normalized percentage [0,1]
      *
      * @details
      * The percentage discern the performance changes from the noise,
      * if the observation difference is below the threshold no
      * action are taken.
      */
    void setNoiseThreshold(float percentage);


    
    /**
     * @brief Check if the constraint is satisfied by the operating point
     * 
     * @param opID Is the id of the operating point
     * 
     */
    bool isAdmissible(OperatingPointID_t opID);
    
    /**
     * @brief Handle the new information from monitor
     * 
     * @param model see the class Model for further details
	 * 
	 * @param currentOP_ID the id of the current operating point
     * 
     * @return If is needed to found another best operating point because
     *         the current OP is no more valid
     * 
     */
    bool updateModel(ModelPtr model, OperatingPointID_t currentOP_ID);
    
    
    /**
     * @brief Return an iterator to the best op
     */
    std::list<OperatingPointID_t>::iterator getBestOP();
    
    
    /**
     * @brief Return an iterator to the worst op
     */
    std::list<OperatingPointID_t>::iterator getWorstOP();
    
    /**
     * @brief Return an iterator to the last valid op
     */
    std::list<OperatingPointID_t>::iterator getLimitOP();
    
    /**
     * @brief Compute the distance between the goal and the op
	 * 
	 * @param op the id of the evaluated operating point
     */
    float getDistance(OperatingPointID_t op);
    
    
    /**
     * @brief Get the value of an Operating Point wrt 
     *        an observable characteristic
     * 
     * @param opID Is the id of the operating point
     */
    double getOPValue(OperatingPointID_t opID);
    
    
    /**
     * @details Set the number of element that an observation
     *          window must have to be considered valid
	 * 
	 * @param elementNumber the minimum element number
	 * 
	 * @brief Set the minimum element number
     */
    void setElementNumber(argo::monitor::WindowSize elementNumber);
    
    
    /**
     * @brief Check the relaxable goal
	 * 
	 * @param currentOP the id of the current Operating Point
	 * 
	 * @details
	 * Since this is a dynamic constraint, the current Operating Point
	 * it's not needed. However it is required by a static constraint
     */
    void checkRelaxable(OperatingPointID_t currentOP);
    
    
    /**
     * @brief Delete all the observation window's elements
     */
    void clearWindow();
    

	/**
	 * @brief Get the name of the field in the Operating Point
	 * 
	 * @return The name of the field
	 */
    std::string getName() { return metricName;}
    
    
  protected:
    
    /**
     * @brief function used to sort element in the guide vector
     */
    static bool sortingFunction(ObservableCharacteristic<dataType>* obj, OperatingPointID_t lhs, OperatingPointID_t rhs);


    /**
     * @brief The observation window used to get
     *        the interest value
     */
    GoalPtr goal;


    /**
     * @details
     * This percentage discern the performance changes from the noise,
     * if the observation difference is below the threshold no
     * action are taken.
     */
    float noisePercentage;
    
    
    /**
     * @details The coefficient error used to correct
     *          the measured revealed form the monitor
     */
    float coefError;
    
    /**
     * @details The list of the OPs ordered wrt
     *          this metric
     */
    std::list<OperatingPointID_t> sortedOPs;
    
    /**
     * @details The result achieved by the operating
     *          points wrt this metric. The index of
     *          the vector is the ID of the OP
     */
    std::vector<dataType> opValues;
    
    /**
     * @details The pointer to op that is the closest op
     *          that satisfy the goal
     */
    std::list<OperatingPointID_t>::iterator limitOP;
    
    
    /**
     * @details The number of element that the observation
     *          window must have to be considered validity
     */
    argo::monitor::WindowSize validityWindow;
    
    
    dataType goalValue;
    
    /**
     * @brief The comparison functor
     */
    ComparisonFunctor comparisonFunction;
    
    /**
     * @details The comparison function of the constraint
     */
    ComparisonFunction cFun;


	/**
	 * @details The data function of the constraint
	 */
	DataFunction dFun;


	/**
	 * @brief The name of the field object of the constraint
	 * 
	 * @details
	 * This is the name of the field of the Operating Point
	 * object of the constraint
	 */
	std::string metricName;
  };
  
  
template <typename dataType>
bool ObservableCharacteristic<dataType>::sortingFunction(ObservableCharacteristic< dataType >* obj, OperatingPointID_t lhs, OperatingPointID_t rhs) {
  return obj->comparisonFunction(obj->opValues[lhs], obj->opValues[rhs]);
}

template <typename dataType>
void ObservableCharacteristic<dataType>::setNoiseThreshold(float percentage) {
    this->noisePercentage = percentage;
}




template <typename dataType>
ObservableCharacteristic<dataType>::ObservableCharacteristic(GoalPtr goal, OperatingPointsList& opList, std::string metricName, bool isRelaxable):goal(goal) {

  //default params
  coefError = 1;
  validityWindow = 1;
  noisePercentage = 0;

  this->metricName = metricName;


  if (unlikely(isRelaxable)) {
      return;
  }

  // check the size of the op list
  if (unlikely( opList.empty())) {
    throw std::logic_error("Error: The operating list is empty!");
  }
  
  // get the value of the ops
  opValues.reserve(opList.size());
  for(std::vector<OperatingPoint>::iterator it = opList.begin(); it != opList.end(); ++it) {
    
    // get the value of the metric of a op
    opValues.push_back(static_cast<dataType>(it->metrics.at(metricName)));
    sortedOPs.push_back(it->id);
  }
  
  // get the goal value and comparison function
  goalValue = goal->getGoalValue();
  cFun = goal->getComparisonFunction();
  dFun = goal->getDataFunction();
  
  // set the comparison functor
  if (cFun == ComparisonFunction::Greater)
    comparisonFunction = std::greater<dataType>();
  
  if (cFun == ComparisonFunction::GreaterOrEqual)
    comparisonFunction = std::greater_equal<dataType>();
  
  if (cFun == ComparisonFunction::Less)
    comparisonFunction = std::less<dataType>();
  
  if (cFun == ComparisonFunction::LessOrEqual)
    comparisonFunction = std::less_equal<dataType>();
  
  // sort the ops
  sortedOPs.sort(std::bind(&ObservableCharacteristic<dataType>::sortingFunction, this, std::placeholders::_1, std::placeholders::_2));
  
  
  // set the limitOP
  limitOP = sortedOPs.begin();
  if (!comparisonFunction(opValues[*limitOP], goalValue)) {
      return;
  }
  std::list<OperatingPointID_t>::iterator it = limitOP;
  for(++it; it != sortedOPs.end(); ++it) {    
    if (comparisonFunction(opValues[*it], goalValue)) {
      ++limitOP;
    } else {
      break;
    }
  }
}



template<typename dataType>
ObservableCharacteristic<dataType>::ObservableCharacteristic(const ObservableCharacteristic &characteristic) {

    // copy the simple attribute
    this->sortingFunction = characteristic.sortingFunction;
    this->goal = characteristic.goal;
    this->noisePercentage = characteristic.noisePercentage;
    this->coefError = characteristic.coefError;
    this->limitOP = characteristic.limitOP;
    this->validityWindow = characteristic.validityWindow;
    this->goalValue = characteristic.goalValue;
    this->comparisonFunction = characteristic.comparisonFunction;
    this->cFun = characteristic.cFun;
    this->metricName = characteristic.metricName;

    // copy the lists
    for(std::list<argo::asrtm::OperatingPointID_t>::const_iterator op = characteristic.sortedOPs.cbegin(); op != characteristic.sortedOPs.cend(); ++op) {
        this->sortedOPs.push_back(*op);
    }

    this->opValues.reserve(characteristic.opValues.size());
    for(std::vector<argo::asrtm::OPMetric_t>::const_iterator op = characteristic.opValues.cbegin(); op != characteristic.opValues.cend(); ++op) {
        this->opValues.push_back(*op);
    }

}


template<typename dataType>
ObservableCharacteristic<dataType>::~ObservableCharacteristic() {
    sortedOPs.clear();
    opValues.clear();
}

  
  
template <typename dataType>
bool ObservableCharacteristic<dataType>::updateModel(ModelPtr model, OperatingPointID_t currentOP_ID) {
  LOG_NEW_LINE("initial situation")
  PRINT_CHARACTERISTIC()
  
  // init the result
  bool result = false;

  // check if the limitOP is valid (in the case that no op satisfy the goal)
  bool limitOPValid = true;
  if (!comparisonFunction(opValues[*limitOP]*coefError, goalValue)) {
    limitOPValid = false;
  }
  
  // ****************************** looking for the observation difference
  // init observation difference
  float observation_difference = 0;
  
  // check if the observed value if it's valid
  if (goal->getObservationSize() >= validityWindow) {
    
    // get the new data
    dataType observedValue = goal->getObservedValue();

    LOG_NEW_LINE("Observed value: " + std::to_string(observedValue));
    LOG_NEW_LINE("Expexted value: " + std::to_string(static_cast<OPMetric_t>(opValues[currentOP_ID]*coefError)));
    
    // compute the model error
    observation_difference = static_cast<float>(observedValue) - static_cast<float>(opValues[currentOP_ID]*coefError);
    
    // compute the new error coefficient
    if (opValues[currentOP_ID] != 0) {
      float coefTemp = static_cast<float>(observedValue)/static_cast<float>(opValues[currentOP_ID]);
      if (std::abs(coefError - coefTemp) > noisePercentage) {
          coefError = coefTemp;
      } else {
          observation_difference = 0;
          LOG_NEW_LINE("The observed difference is just noise, set observed difference to zero");
      }
    } else {
      coefError = 1;
      opValues[currentOP_ID] = static_cast<OPMetric_t>(observedValue);
    }
  }
  
  
  // check if the difference is good or not
  if ((cFun == ComparisonFunction::Less) || (cFun == ComparisonFunction::LessOrEqual)) {
    observation_difference = - observation_difference;  
  }
  LOG_NEW_LINE("observation difference: " + std::to_string(observation_difference))
  
  // ****************************** looking for the goal difference
  
  // get the new goal value
  dataType newGoalValue = goal->getGoalValue();
  
  // compute the goal difference
  float goal_difference = static_cast<float>(newGoalValue) - static_cast<float>(goalValue);
  
  // check if the difference is good or not
  if ((cFun == ComparisonFunction::Greater) || (cFun == ComparisonFunction::GreaterOrEqual)) {
    goal_difference = - goal_difference;
  }
  LOG_NEW_LINE("goal difference: " + std::to_string(goal_difference))
  
  // compute the total difference
  float difference = goal_difference + observation_difference;
  LOG_NEW_LINE("total difference: " + std::to_string(difference))
  
  // set the new goal
  goalValue = newGoalValue;
  
  
  // checking if we need to add or remove OP from S1
  if (difference < 0) {
    
    // *****************  theoretically we need to remove OPs from S1 (the situation get worse)
    LOG_NEW_LINE("checking if we need to remove op from S1")
    ADD_INDENT()
    // checking if the situation actually change
    while(!comparisonFunction(opValues[*limitOP]*coefError, goalValue)) {
      LOG_NEW_LINE("removed op " + std::to_string(*limitOP))

      if (*limitOP == currentOP_ID) {
	      result = true;
      }
      
      model->removeOperatingPointFromS1(*limitOP);
      
      // check boundaries
      if (limitOP == sortedOPs.begin()) {
	      break;
      }
      
      limitOP--;
    }
    REMOVE_INDENT()
    LOG_NEW_LINE("check done")
    
  } else  {
    
    // *****************  theoretically we need to add OP to S2 (the situation get better)
    
    // check if the limitOP is valid (in the case that no op satisfy the goal)
    if (limitOPValid) {
      limitOP++;
    }
    
    // checking if the situation actually change
    LOG_NEW_LINE("checking if we need to add op to S2")
    ADD_INDENT()
    bool isAdded = true;
    while(isAdded) {
      
      // check boundaries
      if (limitOP == sortedOPs.end()) {
	      break;
      }

      INDENT()
      LOG("checking op " + std::to_string(*limitOP))
      
      // check if it satisfy the goal
      isAdded = comparisonFunction(opValues[*limitOP]*coefError, goalValue);
      if (isAdded) {
	      model->addOperatingPoint(*limitOP);
	      limitOP++;
        LOG(", added!")
      } else {
	      isAdded = false;
        LOG(", not valid, skip the other ops")
      }
      NEW_LINE()
      
    }
    REMOVE_INDENT()
    LOG_NEW_LINE("check done")
    
    // because limitOP is the closest op that satisfy the constraint
    if (limitOP != sortedOPs.begin()) {
      limitOP--;
    }
    
    
  }
  LOG_NEW_LINE("new situation:")
  PRINT_CHARACTERISTIC()
  return result;
}

  
template <typename dataType>
bool ObservableCharacteristic<dataType>::isAdmissible(OperatingPointID_t opID) {
  return comparisonFunction(opValues[opID]*coefError, goalValue);
}

template <typename dataType>
double ObservableCharacteristic<dataType>::getOPValue(OperatingPointID_t opID) {
  return static_cast<double>(opValues[opID]*coefError);
}
  
  
  
template <typename dataType>
void ObservableCharacteristic<dataType>::setElementNumber(argo::monitor::WindowSize elementNumber) {
  validityWindow = elementNumber;
}
  
  
template <typename dataType>
void ObservableCharacteristic<dataType>::checkRelaxable(OperatingPointID_t currentOP) {
  // check the relaxable goal only if the window is valid
  if (goal->getObservationSize() >= validityWindow) {
    goal->check();
  }
}


template <typename dataType>
void ObservableCharacteristic<dataType>::clearWindow() {
  goal->clearData();
}


template <typename dataType>
std::list<OperatingPointID_t>::iterator ObservableCharacteristic<dataType>::getWorstOP() {
  std::list<OperatingPointID_t>::iterator result = sortedOPs.end();
  --result;
  return result;  
}

template <typename dataType>
std::list<OperatingPointID_t>::iterator ObservableCharacteristic<dataType>::getBestOP() {
  return sortedOPs.begin();  
}

template <typename dataType>
std::list<OperatingPointID_t>::iterator ObservableCharacteristic<dataType>::getLimitOP() {
  return limitOP;  
}


template <typename dataType>
float ObservableCharacteristic<dataType>::getDistance(OperatingPointID_t op) {
  float diff = static_cast<float>(goalValue) - static_cast<float>(opValues[op])*coefError;
  
  return std::abs(diff);
}

  
}  // namespace asrtm

}  // namespace argo




#endif // ASRTM_OBSERVABLE_CHARACTERISTIC_H
