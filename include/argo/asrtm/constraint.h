/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASRTM_CONSTRAINT_H
#define ASRTM_CONSTRAINT_H

#include <list>
#include <memory>
#include <stdint.h>
#include <string>


#include "argo/asrtm/model.h"
#include "argo/config.h"



namespace argo { namespace asrtm {
  
  /**
   * @brief Constraint general interface
   * 
   * @details
   * This class provides a general interface for a constraint
   * used to define a property that an Operating Point must
   * satisfy, in order to be considered admissible.
   * See the classes ObservableCharacteristic and KnownCharateristic for further details
   */
  class Constraint {
  public:
    
    /**
     * @brief Check if the constraint is satisfied by the operating point
     * 
     * @param opID Is the id of the operating point
     * 
     */
    virtual bool isAdmissible(OperatingPointID_t opID) = 0;
    
    /**
     * @brief Handle the difference w.r.t. the previous situation
     * 
     * @param model see the class Model for further details
     * 
     * @param currentOP_ID the id of the current operating point
     * 
     * @return If is needed to found another best operating point, i.e. if the
     *         current operating point doesn't satisfy anymore the constraint
	 * 
	 * @details
	 * All the constraints compute the difference between the constraint value.
	 * If the constraint is a dynamic one it also compute the difference between
	 * the profiled value of the metric and the observed one. 
     */
    virtual bool updateModel(ModelPtr model,  OperatingPointID_t currentOP_ID) = 0;
    
    
    /**
     * @brief Check the relaxable goal
	 * 
	 * @param currentOP Is the id of the current Operating Point
     */
    virtual void checkRelaxable(OperatingPointID_t currentOP) = 0;
    
    
    /**
     * @brief This method return the worst op for this Constraint.
	 * 
	 * @return The iterator to the worst op for this Constraint
     */
    virtual std::list<OperatingPointID_t>::iterator getWorstOP() = 0;
    
    /**
	 * @brief This method return the best op for this Constraint
	 * 
	 * @return The iterator to the best op for this Constraint
     */
    virtual std::list<OperatingPointID_t>::iterator getBestOP() = 0;
    
    /**
	 * @brief This method return the limit op for this Constraint
	 * 
	 * @return The iterator to the limit op for this Constraint
	 * 
	 * @details
	 * The limit OP is the valid operating point closer to the
	 * constraint value 
     */
    virtual std::list<OperatingPointID_t>::iterator getLimitOP() = 0;
    
    /**
     * @brief Compute the distance between the goal and the op
	 * 
	 * @param opIs the id of the evaluated operating point
	 * 
	 * @return the distance of the op, in percentage w.r.t. the constraint value
     */
    virtual float getDistance(OperatingPointID_t op) = 0;
    
    /**
     * @brief Delete all the observation window's elements
     */
    virtual void clearWindow() = 0;

	/**
	 * @brief Get the name of the field of the OP
	 */
    virtual std::string getName() = 0;

    
    virtual ~Constraint() {}
      
  };
  
  typedef std::shared_ptr<Constraint> ConstraintPtr;

} // namespace asrtm


} // namespace argo

#endif // ASRTM_CONSTRAINT_H
