/*
* Copyright (C) 2012  Politecnico di Milano
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ASRTM_OP_MANAGER_H
#define ASRTM_OP_MANAGER_H

#include <list>
#include <vector>
#include <memory>
#include <string>
#include <mutex>



#include "argo/asrtm/constraint.h"
#include "argo/asrtm/observable_characteristic.h"
#include "argo/asrtm/known_characteristic.h"
#include "argo/asrtm/model.h"
#include "argo/asrtm/operating_point.h"
#include "argo/asrtm/asrtm_enums.h"
#include "argo/config.h"



namespace argo { namespace asrtm {

typedef std::list<ConstraintPtr> ConstraintList;


/**
* @brief  Operating Point Manager
*
* @details
* This class represent a state. It actually implements
* all the operation that Asrtm class provide to the 
* application.
*/
class OPManager {
public:

	
	/**
	* @brief Constructor that create the manager
	* 
	* @param opList is the list of the operating point
	*
	*/
	OPManager(OperatingPointsList opList);
	OPManager(const OPManager& manager);
	~OPManager();
	
	/**
	* @brief Get the best parameters
	*
	* @param changed output parameter, if it is true the best OP is changed
	* 
	* @return The best configuration of the parameters for the current situation
	*/
	ApplicationParameters getBestApplicationParameters(bool* changed = nullptr);
	


	/**
	* @brief  Add a Constraint on the bottom
	* 
	* @param constraint The shared_ptr to the Constraint
	*
	* @details
	* This method is shared between the static and dynamic constraint
	*/
	void addConstraintOnBottom(ConstraintPtr constraint, argo::DataFunction dFun = argo::DataFunction::Average);

	/**
	* @brief  Add a Constraint on the top
	* 
	* @param constraint The shared_ptr to the Constraint
	*
	* @details
	* This method is shared between the static and dynamic constraint
	*/
	void addConstraintOnTop(ConstraintPtr constraint, argo::DataFunction dFun = argo::DataFunction::Average);

	/**
	* @brief  Add a Constraint before another constraint
	* 
	* @param position The position of the constraint used as reference
	* 
	* @param constraint The shared_ptr to the Constraint
	*
	* @details
	* This method is shared between the static and dynamic constraint
	*/
	void addConstraintBefore(ConstraintPosition_t position, ConstraintPtr constraint, argo::DataFunction dFun = argo::DataFunction::Average);

	/**
	* @brief  Add a Constraint after another constraint
	* 
	* @param position The position of the constraint used as reference
	* 
	* @param constraint The shared_ptr to the Constraint
	*
	* @details
	* This method is shared between the static and dynamic constraint
	*/
	void addConstraintAfter(ConstraintPosition_t position, ConstraintPtr constraint, argo::DataFunction dFun = argo::DataFunction::Average);
	
	/**
	* @brief  Add a relaxable Constraint
	* 
	* @param constraint The shared_ptr to the Constraint
	*
	* @details
	* This method is shared between the static and dynamic constraint
	*/
	void addRelaxableConstraint(ConstraintPtr constraint);

	/**
	* @brief  Remove a constraint at the bottom of the list
	*/
	void removeConstraintOnBottom();
	
	/**
	* @brief  Remove a constraint at the top of the list
	*/
	void removeConstraintOnTop();

	/**
	* @brief  Remove a constraint at the given position
	* 
	* @param position The position of the constraint to be removed
	*/
	void removeConstraintAt(ConstraintPosition_t position);

	
	/**
	* @brief  Add a static constraint on the bottom of the constraints list
	* 
	* @param metricName The name of the field that defines the constraint
	* 
	* @param cFun The comparison performed, i.e. greater than or less or equal than.
	* 
	* @param value The actual value of the constraint
	*
	* @details
	* The static constraint does not exploit run-time observation, it is a filter for the
	* Operating Point list.
	*/
	void addStaticConstraintOnBottom(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

	/**
	* @brief  Add a dynamic constraint on the bottom of the constraints list
	* 
	* @param goal The Goal object that defines the constraint
	* 
	* @param metricName The associated metric name in the Operating Point definition
	* 
	* @param noisePercentage Threshold (percentage) that filter small observation fluctuations
	* 
	* @param validityNumber The number of elements that the monitor must have in order to be considered
	*
	* @details
	* The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
	* of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
	* the framework assumes that even the other OPs have a doubled value.
	*/
	template<typename dataType>
	void addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

	/**
	* @brief  Add a static constraint on the top of the constraints list
	* 
	* @param metricName The name of the field that defines the constraint
	* 
	* @param cFun The comparison performed, i.e. greater than or less or equal than.
	* 
	* @param value The actual value of the constraint
	*
	* @details
	* The static constraint does not exploit run-time observation, it is a filter for the
	* Operating Point list.
	*/
	void addStaticConstraintOnTop(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

	/**
	* @brief  Add a dynamic constraint on the top of the constraints list
	* 
	* @param goal The Goal object that defines the constraint
	* 
	* @param metricName The associated metric name in the Operating Point definition
	* 
	* @param noisePercentage Threshold (percentage) that filter small observation fluctuations
	* 
	* @param validityNumber The number of elements that the monitor must have in order to be considered
	*
	* @details
	* The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
	* of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
	* the framework assumes that even the other OPs have a doubled value.
	*/
	template<typename dataType>
	void addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

	/**
	* @brief  Add a static constraint before another constraint
	* 
	* @param postion The position, on the constraints list, of the reference constraint
	* 
	* @param metricName The name of the field that defines the constraint
	* 
	* @param cFun The comparison performed, i.e. greater than or less or equal than.
	* 
	* @param value The actual value of the constraint
	*
	* @details
	* The static constraint does not exploit run-time observation, it is a filter for the
	* Operating Point list.
	* The first position on the constraints list is zero.
	*/
	void addStaticConstraintBefore(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

	/**
	* @brief  Add a dynamic constraint before another constraint
	* 
	* @param postion The position, on the constraints list, of the reference constraint
	* 
	* @param goal The Goal object that defines the constraint
	* 
	* @param metricName The associated metric name in the Operating Point definition
	* 
	* @param noisePercentage Threshold (percentage) that filter small observation fluctuations
	* 
	* @param validityNumber The number of elements that the monitor must have in order to be considered
	*
	* @details
	* The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
	* of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
	* the framework assumes that even the other OPs have a doubled value.
	* The first position on the constraints list is zero.
	*/
	template<typename dataType>
	void addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

	/**
	* @brief  Add a static constraint after another constraint
	* 
	* @param postion The position, on the constraints list, of the reference constraint
	* 
	* @param metricName The name of the field that defines the constraint
	* 
	* @param cFun The comparison performed, i.e. greater than or less or equal than.
	* 
	* @param value The actual value of the constraint
	*
	* @details
	* The static constraint does not exploit run-time observation, it is a filter for the
	* Operating Point list.
	* The first position on the constraints list is zero.
	*/
	void addStaticConstraintAfter(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

	/**
	* @brief  Add a dynamic constraint after another constraint
	* 
	* @param postion The position, on the constraints list, of the reference constraint
	* 
	* @param goal The Goal object that defines the constraint
	* 
	* @param metricName The associated metric name in the Operating Point definition
	* 
	* @param noisePercentage Threshold (percentage) that filter small observation fluctuations
	* 
	* @param validityNumber The number of elements that the monitor must have in order to be considered
	*
	* @details
	* The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
	* of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
	* the framework assumes that even the other OPs have a doubled value.
	* The first position on the constraints list is zero.
	*/
	template<typename dataType>
	void addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

	/**
	* @brief  Add a static relaxable constraint after another constraint
	* 
	* @param metricName The name of the field that defines the constraint
	* 
	* @param cFun The comparison performed, i.e. greater than or less or equal than.
	* 
	* @param value The actual value of the constraint
	* 
	* @param callbackFunction The function called if the constraint is not achieved
	*
	* @details
	* A relaxable constraint doesn't influence the decision algorithm, but if the constraint is not
	* satisfied, it is called a callback function, defined by the developer
	*/
	void addRelaxableConstraint(std::string metricName, ComparisonFunction cFun, OPMetric_t value, std::function<void()> callbackFunction);

	
	/**
	* @brief  Add a dynamic relaxable constraint after another constraint
	* 
	* @param goal The Goal object that defines the constraint
	* 
	* @param metricName The associated metric name in the Operating Point definition
	* 
	* @param validityNumber The number of elements that the monitor must have in order to be considered
	*
	* @details
	* A relaxable constraint doesn't influence the decision algorithm, but if the constraint is not
	* satisfied, it is called a callback function, defined by the developer
	*/
	template<typename dataType>
	void addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber = 1);



	/**
	* 
	* @brief change the value of a static constraint
	* 
	* @param position The position, on the constraints list, of the reference constraint
	* 
	* @param newValue The new value of a constraint
	* 
	* 
	* @details
	* This method changes the value of a static constraint on the current manager.
	* In order to change the goal value of a dynamic constraint, use the Goal class methods.
	* The first position on the constraints list is zero.
	*/
	void changeStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t newValue);

	/**
	* 
	* @brief increment value of a static constraint
	* 
	* @param position The position, on the constraints list, of the reference constraint
	* 
	* @param incPerc The increment value, in percentage
	* 
	* 
	* @details
	* This method increment (in percentage) the value of a static constraint on the current manager.
	* In order to change the goal value of a dynamic constraint, use the Goal class methods.
	* The first position on the constraints list is zero.
	*/
	void incrementStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t incPerc);

	/**
	* 
	* @brief retrieve the goal value of a static constraint
	* 
	* @param position The position, on the constraints list, of the reference constraint
	* 
	* @return The goal value of the selected constraint
	* 
	* 
	* @details
	* This method retrieve the value of a static constraint on the current manager.
	* In order to change the goal value of a dynamic constraint, use the Goal class methods.
	* The first position on the constraints list is zero.
	*/
	double getStaticConstraintGoalValue(ConstraintPosition_t position);


	/**
	* @brief  Set a geometric rank function
	* 
	* @param structure The map < field_name, weight> that define the rank
	* 
	* @param objective Tells if the best OP maximize or minimize the rank value
	*
	* @details
	* The geometric rank is in the form (op[field_name_n]^(weight_n))*(op[field_name_n+1]^(weight_n+1))*...
	* The field_name can address both metric and parameters. It is possible to define
	* positive and negative weight, however it is dangerous if the value of the field
	* contains both positive and negative values.
	* The rank value is a one-shoe computation, so it doesn't affect the Run-Time performance
	*/
	void setGeometricRank(std::map<std::string, float> structure, RankObjective objective);

	/**
	* @brief  Set a linear rank function
	* 
	* @param structure The map < field_name, weight> that define the rank
	* 
	* @param objective Tells if the best OP maximize or minimize the rank value
	*
	* @details
	* The geometric rank is in the form (op[field_name_n]*(weight_n))+(op[field_name_n+1]*(weight_n+1))*...
	* The field_name can address both metric and parameters. It is possible to define
	* positive and negative weight, however it is dangerous if the value of different fields
	* have different magnitude.
	* The rank value is a one-shoe computation, so it doesn't affect the Run-Time performance
	*/
	void setLinearRank(std::map<std::string, float> structure, RankObjective objective);

	/**
	* @brief  Set a simple rank
	* 
	* @param metricName The name of the field that defines the rank
	* 
	* @param objective Tells if the best OP maximize or minimize the rank value
	*
	*/
	void setSimpleRank(std::string metricName, RankObjective objective);

	/**
	* @details
	* When an op is changed, all the observation window
	* involved must be cleared from the previous data
	*/
	void resetWindows();

	/**
	* @brief get the value of the metric wrt the current operating point
	* @param metricName the name of the selected metric
	* @return the value of the selected metric
	*/
	OPMetric_t getMetricValue(std::string metricName);


	/**
	* @brief check if the model wrt the current state
	* @return true if at least one operating point satisfies all the constraints
	*/
	bool isPossible();
	
	
protected:
	
	
	/**
	* @brief  Add a constraint in the constraint list
	* 
	* @param constraint The constraint to be added
	* 
	* @param position The iterator to the position
	* 
	* @details
	* The iterator is used in the insert() method
	* of the list
	*
	*/
	void addConstraint(argo::asrtm::ConstraintPtr constraint, std::list< argo::asrtm::ConstraintPtr >::iterator position, argo::DataFunction dFun = argo::DataFunction::Average);

	/**
	* @brief  Remove a constraint
	* 
	* @param position The iterator to the constraint to be removed
	*
	*/
	void removeConstraint(ConstraintList::iterator position);
	
	
	
	
	
	/**
	* @brief Check if an op satisfies the constraints
	* 
	* @param opID The id of the evaluated operating point
	* 
	* @param type If the op must satisfies all the constraints or
	*                 only the ones above the unsatisfiable constraint
	* 
	* @return True if the Operating Point is valid
	*/
	bool isValid(OperatingPointID_t opID, CheckType type);
	
	
	/**
	* @brief Get the best from two operating point
	* 
	* @param opID1 The id of the first operating point
	* 
	* @param opID2 The id of the second operating point
	* 
	* @return The if id of the best operating point
	* 
	*/
	OperatingPointID_t getBest(OperatingPointID_t opID1, OperatingPointID_t opID2);
	
	
	
	/**
	* @details It's the list of ApplicationParameters,
	*          the index of the vector is the ID of
	*          the OP. This list is not sorted, nor
	*          modified
	* 
	* @brief The list of the application parameters
	*/
	std::vector<ApplicationParameters> appParams;
	
	
	/**
	* 
	* @brief The model used by the OPManager
	* 
	* 
	* @details 
	* This is used to represent the situation and
	* take decision about the best operating point
	*/
	ModelPtr model;

	
	/**
	* @brief The constraints that an OP must satisfy
	*/
	ConstraintList constraints;
	
	/**
	* @brief The constraints that the manager can relax
	*/
	ConstraintList relaxableConstraints;
	
	
	/**
	* @brief How much an operating point is good
	*/
	std::vector<Rank_t> ranks;
	
	/**
	* @brief The id of the current best Operating Point
	*/
	OperatingPointID_t currentBestOP;
	
	/**
	* @details
	* This iterator represent the constraint that invalidates all
	* the op in S1.
	* 
	* @brief The reference to the unsatisfiable constraint
	*/
	ConstraintList::iterator unsatisfiableConstraint;
    
    /**
     * @brief Used to get a best op when a constraint empty the set S1
     */
    OperatingPointID_t getBestOPFromUnsatisfiableConstraint();
    
    
    /**
     * @details
     * The best op is retrieved from S1
     */
    OperatingPointID_t getBestOPFromModel();
    
    
    
    /**
     * @brief Checks if the relaxable constraints are achieved
     */
    void checkRelaxableConstraints();
    
    

	/**
	* @details
	* This list contains all the operating points that this
	* manager must handle
	* 
	* @brief The Operating Points list
	*/
	OperatingPointsList opList;
	
	
	/**
	* @brief Mutex variable associated to the constraints
	*/
	std::mutex constraintMutex;
	
};

typedef std::shared_ptr<OPManager> OPManagerPtr;


template<typename dataType>
void OPManager::addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
// create the ObsrvableCharacteristic
std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
oc->setElementNumber(validityNumber);
oc->setNoiseThreshold(noisePercentage);

// add the constraint
addConstraintAfter(position, std::dynamic_pointer_cast<Constraint>(oc), goal->getDataFunction());
}

template<typename dataType>
void OPManager::addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
// create the ObsrvableCharacteristic
std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
oc->setElementNumber(validityNumber);
oc->setNoiseThreshold(noisePercentage);

// add the constraint
addConstraintBefore(position, std::dynamic_pointer_cast<Constraint>(oc), goal->getDataFunction());
}


template<typename dataType>
void OPManager::addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
// create the ObsrvableCharacteristic
std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
oc->setElementNumber(validityNumber);
oc->setNoiseThreshold(noisePercentage);

// add the constraint
addConstraintOnTop(std::dynamic_pointer_cast<Constraint>(oc), goal->getDataFunction());
}

template<typename dataType>
void OPManager::addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
// create the ObsrvableCharacteristic
std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
oc->setElementNumber(validityNumber);
oc->setNoiseThreshold(noisePercentage);

// add the constraint
addConstraintOnBottom(std::dynamic_pointer_cast<Constraint>(oc), goal->getDataFunction());
}

template<typename dataType>
void OPManager::addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber) {
// create the ObservableCharacteristic
std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName, true));
oc->setElementNumber(validityNumber);

// add the constraint
addRelaxableConstraint(std::dynamic_pointer_cast<Constraint>(oc), goal->getDataFunction());
}

} // namespace asrtm

} // namespace argo

#endif // ASRTM_OP_MANAGER_H
