/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef ASRTM_ASRTM_H
#define ASRTM_ASRTM_H

#include <list>
#include <string>
#include <map>
#include <memory>

#include "argo/config.h"
#include "argo/asrtm/operating_point.h"
#include "argo/asrtm/op_manager.h"


/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo { 
	
	

/**
 * @brief  The sub namespace that wrap the Application-Specific Run-Time Manager
 * 
 */	
namespace asrtm {


	/**
	 * @brief  Application-Specific Run-Time Manager
	 *
	 * @details
	 * This class provides a wrapper for all the function that the module. The developer
	 * use only the methods provided by this class. This class provide the methods that
	 * handle the constraints, the rank definition, the state changes and the method that
	 * retrieve the best application parameter.
	 */
	class Asrtm {
	public:
		
		/**
		 * @brief  Default constructor
		 * 
		 * @param opList Input parameter that store all the Operating Points
		 *
		 * @details
		 * This method store the list of Operating Points and automatically create and 
		 * populate the States. By default an Operating Point belong to the "Default" state,
		 * if the OP don't explicitly list any state.
		 * If there are more than one state, the initial state is the first state that
		 * is stated in the OPs list. However is safer to proper set the initial state
		 * after the asrtm construction
		 */
		Asrtm(const OperatingPointsList& opList);
        Asrtm(const Asrtm& asrtm);
        ~Asrtm();

		/**
		 * @brief  Change the current state of the asrtm
		 * 
		 * @param newState The name of the state to choose
		 *
		 * @details
		 * After this call, all the methods affect only the newState. So in order to
		 * define the behavior of more than one state, it is needed to define the
		 * constraints and the rank of every state independently
		 */
		void changeState(std::string newState);

        /**
         * @brief Get the best parameters from the current state
         *
         * @param changed output parameter, if it is true the best OP is changed
		 * 
		 * @return The best configuration of the parameters for the current situation
         */
        ApplicationParameters getBestApplicationParameters(bool* changed = nullptr);

		/**
		 * @brief  Set a geometric rank function
		 * 
		 * @param structure The map < field_name, weight> that define the rank
		 * 
		 * @param objective Tells if the best OP maximize or minimize the rank value
		 *
		 * @details
		 * The geometric rank is in the form (op[field_name_n]^(weight_n))*(op[field_name_n+1]^(weight_n+1))*...
		 * The field_name can address both metric and parameters. It is possible to define
		 * positive and negative weight, however it is dangerous if the value of the field
		 * contains both positive and negative values.
		 * The rank value is a one-shoe computation, so it doesn't affect the Run-Time performance
		 */
        void setGeometricRank(std::map<std::string, float> structure, RankObjective objective);

		/**
		 * @brief  Set a linear rank function
		 * 
		 * @param structure The map < field_name, weight> that define the rank
		 * 
		 * @param objective Tells if the best OP maximize or minimize the rank value
		 *
		 * @details
		 * The geometric rank is in the form (op[field_name_n]*(weight_n))+(op[field_name_n+1]*(weight_n+1))*...
		 * The field_name can address both metric and parameters. It is possible to define
		 * positive and negative weight, however it is dangerous if the value of different fields
		 * have different magnitude.
		 * The rank value is a one-shoe computation, so it doesn't affect the Run-Time performance
		 */
        void setLinearRank(std::map<std::string, float> structure, RankObjective objective);

		
		/**
		 * @brief  Set a simple rank
		 * 
		 * @param metricName The name of the field that defines the rank
		 * 
		 * @param objective Tells if the best OP maximize or minimize the rank value
		 *
		 */
        void setSimpleRank(std::string metricName, RankObjective objective);

		
		/**
		 * @brief  Add a static constraint on the bottom of the constraints list
		 * 
		 * @param metricName The name of the field that defines the constraint
		 * 
		 * @param cFun The comparison performed, i.e. greater than or less or equal than.
		 * 
		 * @param value The actual value of the constraint
		 *
		 * @details
		 * The static constraint does not exploit run-time observation, it is a filter for the
		 * Operating Point list.
		 */
        void addStaticConstraintOnBottom(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

		/**
		 * @brief  Add a dynamic constraint on the bottom of the constraints list
		 * 
		 * @param goal The Goal object that defines the constraint
		 * 
		 * @param metricName The associated metric name in the Operating Point definition
		 * 
		 * @param noisePercentage Threshold (percentage) that filter small observation fluctuations
		 * 
		 * @param validityNumber The number of elements that the monitor must have in order to be considered
		 *
		 * @details
		 * The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
		 * of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
		 * the framework assumes that even the other OPs have a doubled value.
		 */
		template<typename dataType>
        void addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);





		/**
		 * @brief  Add a static constraint on the top of the constraints list
		 * 
		 * @param metricName The name of the field that defines the constraint
		 * 
		 * @param cFun The comparison performed, i.e. greater than or less or equal than.
		 * 
		 * @param value The actual value of the constraint
		 *
		 * @details
		 * The static constraint does not exploit run-time observations, it is a filter for the
		 * Operating Point list.
		 */
        void addStaticConstraintOnTop(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

		
		/**
		 * @brief  Add a dynamic constraint on the top of the constraints list
		 * 
		 * @param goal The Goal object that defines the constraint
		 * 
		 * @param metricName The associated metric name in the Operating Point definition
		 * 
		 * @param noisePercentage Threshold (percentage) that filter small observation fluctuations
		 * 
		 * @param validityNumber The number of elements that the monitor must have in order to be considered
		 *
		 * @details
		 * The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
		 * of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
		 * the framework assumes that even the other OPs have a doubled value.
		 */
		template<typename dataType>
        void addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);





		/**
		 * @brief  Add a static constraint before another constraint
		 * 
		 * @param postion The position, on the constraints list, of the reference constraint
		 * 
		 * @param metricName The name of the field that defines the constraint
		 * 
		 * @param cFun The comparison performed, i.e. greater than or less or equal than.
		 * 
		 * @param value The actual value of the constraint
		 *
		 * @details
		 * The static constraint does not exploit run-time observations, it is a filter for the
		 * Operating Point list.
		 * The first position on the constraints list is zero.
		 */
        void addStaticConstraintBefore(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

		/**
		 * @brief  Add a dynamic constraint before another constraint
		 * 
		 * @param postion The position, on the constraints list, of the reference constraint
		 * 
		 * @param goal The Goal object that defines the constraint
		 * 
		 * @param metricName The associated metric name in the Operating Point definition
		 * 
		 * @param noisePercentage Threshold (percentage) that filter small observation fluctuations
		 * 
		 * @param validityNumber The number of elements that the monitor must have in order to be considered
		 *
		 * @details
		 * The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
		 * of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
		 * the framework assumes that even the other OPs have a doubled value.
		 * The first position on the constraints list is zero.
		 */
		template<typename dataType>
        void addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);




		/**
		 * @brief  Add a static constraint after another constraint
		 * 
		 * @param postion The position, on the constraints list, of the reference constraint
		 * 
		 * @param metricName The name of the field that defines the constraint
		 * 
		 * @param cFun The comparison performed, i.e. greater than or less or equal than.
		 * 
		 * @param value The actual value of the constraint
		 *
		 * @details
		 * The static constraint does not exploit run-time observation, it is a filter for the
		 * Operating Point list.
		 * The first position on the constraints list is zero.
		 */
        void addStaticConstraintAfter(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

		/**
		 * @brief  Add a dynamic constraint after another constraint
		 * 
		 * @param postion The position, on the constraints list, of the reference constraint
		 * 
		 * @param goal The Goal object that defines the constraint
		 * 
		 * @param metricName The associated metric name in the Operating Point definition
		 * 
		 * @param noisePercentage Threshold (percentage) that filter small observation fluctuations
		 * 
		 * @param validityNumber The number of elements that the monitor must have in order to be considered
		 *
		 * @details
		 * The dynamic constraint use a monitor to observe at Run-Time the metric. The asrtm use a linear propagation
		 * of the error. For example if the value profiled at Design-Time is double w.r.t. the one observed at Run-Time,
		 * the framework assumes that even the other OPs have a doubled value.
		 * The first position on the constraints list is zero.
		 */
		template<typename dataType>
        void addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);




		/**
		 * @brief  Add a static relaxable constraint after another constraint
		 * 
		 * @param metricName The name of the field that defines the constraint
		 * 
		 * @param cFun The comparison performed, i.e. greater than or less or equal than.
		 * 
		 * @param value The actual value of the constraint
		 * 
		 * @param callbackFunction The function called if the constraint is not achieved
		 *
		 * @details
		 * A relaxable constraint doesn't influence the decision algorithm, but if the constraint is not
		 * satisfied, it is called a callback function, defined by the developer
		 */
        void addRelaxableConstraint(std::string metricName, ComparisonFunction cFun, OPMetric_t value, std::function<void()> callbackFunction);

		/**
		 * @brief  Add a dynamic relaxable constraint after another constraint
		 * 
		 * @param goal The Goal object that defines the constraint
		 * 
		 * @param metricName The associated metric name in the Operating Point definition
		 * 
		 * @param validityNumber The number of elements that the monitor must have in order to be considered
		 *
		 * @details
		 * A relaxable constraint doesn't influence the decision algorithm, but if the constraint is not
		 * satisfied, it is called a callback function, defined by the developer
		 */
        template<typename dataType>
        void addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber = 1);



		/**
		 * @brief  Remove the constraint at the bottom of the constraints list
		 */
		void removeConstraintOnBottom();
		
		/**
		 * @brief  Remove the constraint at the top of the constraints list
		 */
		void removeConstraintOnTop();

		/**
		 * @brief  Remove the constraint at the given position
		 * 
		 * @param position The position, on the constraints list, of the reference constraint
		 * 
		 * @details
		 * The first position on the constraints list is zero.
		 */
		void removeConstraintAt(ConstraintPosition_t position);


		/**
		 * 
		 * @brief change the value of a static constraint
		 * 
		 * @param position The position, on the constraints list, of the reference constraint
		 * 
		 * @param newValue The new value of a constraint
		 * 
		 * 
		 * @details
		 * This method changes the value of a static constraint on the current manager.
		 * In order to change te goal value of a dynamic constraint, use the Goal class methods.
		 * The first position on the constraints list is zero.
		 */
		void changeStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t newValue);

		/**
		 * 
		 * @brief increment value of a static constraint
		 * 
		 * @param position The position, on the constraints list, of the reference constraint
		 * 
		 * @param incPerc The increment value, in percentage
		 * 
		 * 
		 * @details
		 * This method increment (in percentage) the value of a static constraint on the current manager.
		 * In order to change the goal value of a dynamic constraint, use the Goal class methods.
		 * The first position on the constraints list is zero.
		 */
		void incrementStaticConstraintGoalValue(ConstraintPosition_t position, float incPerc);

		/**
		 * 
		 * @brief retrieve the goal value of a static constraint
		 * 
		 * @param position The position, on the constraints list, of the reference constraint
		 * 
		 * @return The goal value of the selected constraint
		 * 
		 * 
		 * @details
		 * This method retrieve the value of a static constraint on the current manager.
		 * In order to change the goal value of a dynamic constraint, use the Goal class methods.
		 * The first position on the constraints list is zero.
		 */
		double getStaticConstraintValue(ConstraintPosition_t position);


        /**
         * @brief get the value of the metric wrt the current operating point
         * @param metricName the name of the selected metric
         * @return the metric value in the current Operating Point
		 * @details
		 * The value is the one stored in the Operating Point.
         */
        OPMetric_t getMetricValue(std::string metricName);

        /**
         * @brief check if all the constraints are satisfied
		 * 
         * @return true if at least one Operating Point satisfies all the constraints
         */
        bool isPossible();

		
	private:
		
		/**
		 * @brief the list of OPManager
		 * 
		 * @details
		 * A state is identified using its name, thus in this map for each state is
		 * associated an OPManager.
		 */
		std::map <std::string, OPManagerPtr> managers;


		/**
		 * @brief a reference to the current OPManager
		 */
		OPManagerPtr currentManager;
	};


    typedef std::shared_ptr<argo::asrtm::Asrtm> AsrtmPtr;


	template<typename dataType>
    void Asrtm::addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, monitor::WindowSize validityNumber) {
        currentManager->addDynamicConstraintOnBottom<dataType>(goal, metricName, noisePercentage, validityNumber);
	}





	template<typename dataType>
    void Asrtm::addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, monitor::WindowSize validityNumber) {
        currentManager->addDynamicConstraintOnTop<dataType>(goal, metricName, noisePercentage, validityNumber);
	}




	template<typename dataType>
    void Asrtm::addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
        currentManager->addDynamicConstraintBefore<dataType>(position, goal, metricName, noisePercentage, validityNumber);
	}





	template<typename dataType>
    void Asrtm::addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
        currentManager->addDynamicConstraintAfter<dataType>(position, goal, metricName, noisePercentage, validityNumber);
	}




	template<typename dataType>
    void Asrtm::addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber ) {
        currentManager->addRelaxableConstraint(goal, metricName, validityNumber);
    }




} // namespace asrtm

} // namespace argo


 #endif // ASRTM_ASRTM_H
