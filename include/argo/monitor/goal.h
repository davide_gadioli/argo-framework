#ifndef ARGO_MONITOR_GOAL_H
#define ARGO_MONITOR_GOAL_H

#include<algorithm>
#include<stdexcept>
#include<memory>

#include "argo/monitor/data_buffer.h"



/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {

enum class ComparisonFunction:uint8_t {
    Greater = 0,
    Less = 1,
    GreaterOrEqual = 2,
    LessOrEqual = 3
};


/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */	
namespace monitor {



/**
 * @brief  The goal that a monitored metric must reach
 * 
 * @template The type of the monitor
 *
 * @details
 * This class represent a goal that a monitored metric must reach.
 * It provides all the methods to check, evaluate and change the goal value.
 * If it used with the Asrtm, any changes must be done on this object
 */
template<typename dataType>
class Goal {
public:

    typedef std::shared_ptr<DataBuffer<dataType>> DataBufferPtr;

	/**
	 * @brief  Construct a goal
	 * 
	 * @param data_function The interested property, like average or variance
	 * 
	 * @param comparison_function The comparison performed, i.e. greater than or less or equal than.
	 * 
	 * @param value The actual value of the goal
	 */
    Goal(DataBufferPtr observation_window, DataFunction data_function, ComparisonFunction comparison_function, dataType value);
    ~Goal();


    // setters
	/**
	 * @brief  Set a new goal value
	 * 
	 * @param new_value The new value of the goal
	 * 
	 * @param value The actual value of the goal
	 */
    void setGoalValue(dataType new_value) { value = new_value; }
    
    /**
	 * @brief  Increment the value of the goal
	 * 
	 * @param new_value The increment (in percentage) of the goal value
	 */
    void incrementGoalValue(float increment) { value = static_cast<dataType>(value*increment); }
    
    /**
	 * @brief  Set a callback function
	 * 
	 * @param my_func The application function that must be called
	 * 
	 * @details
	 * The function is called only if the goal is not reached when it is called
	 * the method check. Pushing new data in the monitor don't cause a call to the
	 * function.
	 */
    void setCallbackFunction(std::function<void()> my_func) { callBackFunction = my_func; }



    // getters
    
    /**
	 * @brief  Get the goal value
	 * 
	 * @return the goal value 
	 */
    dataType getGoalValue() { return value; }
    
    /**
	 * @brief  Get the value observed by the monitor
	 * 
	 * @return the observed value
	 * 
	 * @details
	 * The statistical property extracted is the one defined when the Goal is created
	 */
    dataType getObservedValue() { return observation_window->get(data_function); }
    
    /**
	 * @brief  Check if the goal is reached
	 * 
	 * @return return True if the goal is reached
	 * 
	 * @details
	 * If the goal is relaxable and it doesn't reach the goal value, a callback function is called
	 */
    bool check();
	
	/**
	 * @brief  Compute the Normalized Actual Penalty 
	 * 
	 * @return the Normalized Actual Penalty 
	 * 
	 * @details
	 * The Normalized Actual Penalty is defined as |goal_value - observed_value|/( goal_value + observed_value),
	 */
    float getNAP();
	
	/**
	 * @brief  Get the relative error 
	 * 
	 * @return the relative error
	 */
    float getRelativeError();
	
	/**
	 * @brief  Get the absolute error 
	 * 
	 * @return the absolute error
	 */
    dataType getAbsoluteError();
	
	
	/**
	 * @brief  Get the comparison function
	 * 
	 * @return the comparison function
	 */
    ComparisonFunction getComparisonFunction() { return comparison_function; }
    
    
    /**
	 * @brief  Get the data function
	 * 
	 * @return the data function
	 */
	DataFunction getDataFunction() { return data_function; }


    // observation window methods
    /**
	 * @brief  Clear the monitor's data buffer
	 */	
    void clearData() { observation_window->clear(); }
    
    /**
	 * @brief  Get the number of element stored in the monitor
	 * 
	 * @return the number of element stored in the monitor
	 */	
    WindowSize getObservationSize() { return observation_window->size(); }


   


private:

    // used for internal checking, it doesn't trigger any callbackfunction
    bool isAchieved();

    // the reference to the observation window
    DataBufferPtr observation_window;


    // the comparison function
    std::function<bool(dataType,dataType)> comparator;

    // the goal value
    dataType value;
	
	// the goal's function
	DataFunction data_function;
	ComparisonFunction comparison_function;
	


    std::function<void()> callBackFunction;

};



template<typename dataType>
Goal<dataType>::Goal(DataBufferPtr observation_window, DataFunction data_function, ComparisonFunction comparison_function, dataType value) {
	
	// check if the window is a nullptr
	if (observation_window == nullptr )
	{
		throw std::runtime_error("Attempting to create a goal with a null window");
	}

    // set the reference to the observation window
    this->observation_window = observation_window;

    // set the goal value
    this->value = value;

    // set the data function
    this->data_function = data_function;

    // set the comparison function
    this->comparison_function = comparison_function;

    callBackFunction = nullptr;

    // set the appropriate comparison function
    switch(comparison_function) {
    case(ComparisonFunction::Greater):
        comparator = std::greater<dataType>();
        break;
    case(ComparisonFunction::Less):
        comparator = std::less<dataType>();
        break;
    case(ComparisonFunction::GreaterOrEqual):
        comparator = std::greater_equal<dataType>();
        break;
    case(ComparisonFunction::LessOrEqual):
        comparator = std::less_equal<dataType>();
        break;
    default:
        throw std::logic_error("Defensive programming: reached unreachable statement in goal creation");
    }

}


template<typename dataType>
bool Goal<dataType>::check() {

    bool result = comparator(observation_window->get(data_function), value);

    if (!result && (callBackFunction != nullptr)) {
        callBackFunction();
    }

    return result;
}

template<typename dataType>
bool Goal<dataType>::isAchieved() {

    return comparator(observation_window->get(data_function), value);
}


template<typename dataType>
float Goal<dataType>::getNAP() {

    if (isAchieved()) {
        return 0;
    }


    dataType observed_value = observation_window->get(data_function);

    dataType first_operator = std::max<dataType>(value, observed_value);
    dataType second_operator = std::min<dataType>(value, observed_value);

    float difference = std::abs(static_cast<float>(first_operator - second_operator));
    float summ = std::abs(static_cast<float>(first_operator + second_operator));

    if (summ == 0 ) {
        return difference;
    }

    return difference/summ;
}




template<typename dataType>
float Goal<dataType>::getRelativeError() {

    if (isAchieved()) {
        return 0;
    }

    float observed_value = static_cast<float>(observation_window->get(data_function));

    if (observed_value == 0) {
        return std::abs(value);
    }

    return std::abs(1 - (static_cast<float>(value) / observed_value));

}


template<typename dataType>
dataType Goal<dataType>::getAbsoluteError() {

    if (isAchieved()) {
        return 0;
    }

    dataType observed_value = observation_window->get(data_function);

    dataType first_operator = std::max<dataType>(value, observed_value);
    dataType second_operator = std::min<dataType>(value, observed_value);


    return std::abs(first_operator - second_operator);
}


template<typename dataType>
Goal<dataType>::~Goal() {
    observation_window->clear();
}


} // namespace Monitor
} // namespace Argo


#endif //ARGO_MONITOR_GENERIC_WINDOW_H
