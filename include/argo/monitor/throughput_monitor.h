/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_MONITOR_THROUGHPUT_MONITOR_H_
#define ARGO_MONITOR_THROUGHPUT_MONITOR_H_

#include <chrono>
#include <memory>

#include "argo/config.h"
#include "argo/monitor/data_buffer.h"
#include "argo/monitor/goal.h"


/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {
	
/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */
namespace monitor {



/**
 * @brief  The throughput monitor
 *
 * @details
 * This use the std::chrono interface to gather the execution time
 * using a steady clock, if available. Otherwise it use the monotonic_clock
 * 
 * @note
 * The throughput is measured as [data]/seconds
 */
class ThroughputMonitor: public DataBuffer<Throughput_t> {
public:

	/**
	 * @brief  Default constructor
	 * 
	 * @param window_size The dimension of the observation window
	 * 
	 */
    ThroughputMonitor(WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);

	/**
	 * @brief  Start the observation
	 * 
	 */
    void start();


	/**
	 * @brief  Stop the observation and push the new data in the buffer
	 * 
	 * @param data The number of data processed in the observed execution time
	 * 
	 */
    void stop(Data_elaborated_t data);


private:

    // initial time measure
    std::chrono::steady_clock::time_point tStart;


    // flag for the measuring state
    bool started;

};


typedef std::shared_ptr<argo::monitor::ThroughputMonitor> ThroughputMonitorPtr;


typedef argo::monitor::Goal<argo::monitor::Throughput_t> ThroughputGoal;
typedef std::shared_ptr<ThroughputGoal> ThroughputGoalPtr;




} // namespace monitor


} // namespace argo

#endif /* ARGO_MONITOR_THROUGHPUT_MONITOR_H_ */
