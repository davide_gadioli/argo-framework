#ifndef ARGO_MONITOR_DATA_BUFFER_H
#define ARGO_MONITOR_DATA_BUFFER_H

#include <list>
#include <vector>
#include <cstdint>
#include <mutex>
#include <memory>
#include <iostream>


#include "argo/config.h"


#define RESET_COUNTER 100

#define ARGO_DEFAULT_BUFFER_SIZE 1



/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {

enum class DataFunction:uint8_t {
    Average = 0,
    Variance = 1,
    Min = 2,
    Max = 3
};

/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */	
namespace monitor {



/**
 * @brief  A base container of the observed data
 * 
 * @template The type of the stored data
 *
 * @details
 * This class is a container for the observed data. It is the base class for
 * every monitor. It provide the methods for manipulating the observation window
 * ( the data buffer ) and for extracting statistical properties.
 * If the developer need to implement a custom monitor, this is
 * the class that must be extended in order to interact with the framework
 */
template<typename dataType>
class DataBuffer {
public:
	
	/**
	 * @brief  Construct a data buffer
	 * 
	 * @param observation_size The size of the buffer, default 1
	 *
	 * @details
	 * A Data Buffer represents a single observation, if the developer need
	 * to observe more metric, another buffer must be created
	 */
    DataBuffer(WindowSize observation_size = ARGO_DEFAULT_BUFFER_SIZE);
    DataBuffer(const DataBuffer& data_buffer);
    ~DataBuffer();


    // --------------   window manipulator methods
	
	/**
	 * @brief  Insert an element in the buffer
	 * 
	 * @param data The data to be pushed in the buffer
	 *
	 * @details
	 * The Data Buffer is actually a circular buffer, thus any element
	 * push when the buffer is full, replace the older
	 */
    void push(dataType data);
	
	/**
	 * @brief  Remove all the elements stored
	 */
    void clear();
	
	/**
	 * @brief  Change the size of the buffer
	 * 
	 * @param new_size The new size of the buffer
	 * 
	 * @details
	 * If the new size is less than than the current, the oldest
	 * element will be erased.
	 */
    void resize(WindowSize new_size);
	
	/**
	 * @brief  Check if the buffer is empty
	 * 
	 * @return return True if the data_buffer is empty
	 */
    inline bool empty() { return data_buffer.empty(); }
    
    /**
	 * @brief  Check if the buffer is full
	 * 
	 * @return return True if the data_buffer is full
	 */
    inline bool full() { return data_buffer.size() == max_size; }
    
    /**
	 * @brief  Get the newest element pushed in the data buffer
	 * 
	 * @return the newest element pushed in the data buffer
	 */
    inline dataType getLastElement() { return data_buffer.back(); }
    
    /**
	 * @brief  Get the max size available for the buffer
	 * 
	 * @return the max size available for the buffer
	 */
    inline WindowSize getBufferMaxSize() { return max_size; }
    
    /**
	 * @brief  Get the current size of the buffer
	 * 
	 * @return the current size of the buffer
	 */
    inline WindowSize size() { return data_buffer.size(); }


    // --------------  public interface for getting the statistical properties
    
    /**
	 * @brief  Extract a statistical property over the stored data
	 * 
	 * @param what The interested property, like average or variance
	 * 
	 * @details
	 * The available properties are Average, Variance, Max and Min
	 */
    dataType get(DataFunction what);



private:

    // -------------- statistical methods
    dataType getAverage();
    dataType getVariance();
    dataType getMin();
    dataType getMax();


    // --------------- the extractor functions
    const std::vector<dataType (DataBuffer<dataType>::*)()> extractors;


    // --------  Buffer size variables
    WindowSize max_size;

    // -------- Internal structure variables
    std::list<dataType> data_buffer;

    // -------- Statistical measure
    float summ;
    dataType min;
    dataType max;

    // -------- Optimization flag
    dataType variance;
    CounterSize average_counter;
    bool min_available;
    bool max_available;
    bool variance_available;

    // -------- Mutex
    std::mutex buffer_mutex;
};




// -------------------------------- Manipulator methods

#define __ARGO_EXTRACTORS\
                 { &DataBuffer<dataType>::getAverage,\
                   &DataBuffer<dataType>::getVariance,\
                   &DataBuffer<dataType>::getMin,\
                   &DataBuffer<dataType>::getMax        }

template<typename dataType>
DataBuffer<dataType>::DataBuffer(WindowSize observation_size):extractors(  __ARGO_EXTRACTORS  )

{
    // one time initiliazation
    max_size = observation_size;


    // reset the internal variable
    summ = 0;
    average_counter = 0;
    variance_available = false;
    variance = 0;
    min_available = false;
    max_available = false;
}




template<typename dataType>
void DataBuffer<dataType>::clear() {

    // acquire the lock
    std::lock_guard<std::mutex> lock(buffer_mutex);

    // clear the data list
    data_buffer.clear();

    // reset the internal variable
    summ = 0;
    average_counter = 0;
    variance_available = false;
    variance = 0;
    min_available = false;
    max_available = false;
}




template<typename dataType>
void DataBuffer<dataType>::resize(WindowSize new_size) {

    // acquire the lock
    std::lock_guard<std::mutex> lock(buffer_mutex);

    if (new_size == 0) {
        throw std::runtime_error("Resizing databuffer to zero elements!");
    }


    // check how many data we need to pop
    if ( new_size < static_cast<WindowSize>(data_buffer.size()) ) {

        WindowSize delta = data_buffer.size() - new_size;


        for(WindowSize counter = 0; counter < delta; counter++) {
            // decrement the summatory
            summ -= data_buffer.front();

            // actually erase the data
            data_buffer.erase(data_buffer.begin());
        }

    }

    // set the new size
    max_size = new_size;
}





template<typename dataType>
void DataBuffer<dataType>::push(dataType data) {

    // acquire the lock
    std::lock_guard<std::mutex> lock(buffer_mutex);

    // update the min/max if possible
    if (data_buffer.empty()) {
        min = data;
        max = data;
        min_available = true;
        max_available = true;
    } else {
        if (data < min) {
            min = data;
            min_available = true;
        }
        if (data > max) {
            max = data;
            max_available = false;
        }
    }

    // check if we need to pop the last element
    if (data_buffer.size() == max_size) {

        // pop the element
        dataType element = data_buffer.front();
        data_buffer.erase(data_buffer.begin());

        // update the average
        summ -= element;

        // update the average counter
        average_counter++;

        // check for the min/max change
        if (element == max) {
            max_available = false;
        }

        if (element == min) {
            max_available = false;
        }

    }

    // insert the data
    data_buffer.push_back(data);

    // update the summ of value
    summ += data;

    // update the flag
    variance_available = false;
}






// -------------------------------- Statistical methods


template<typename dataType>
dataType DataBuffer<dataType>::get(DataFunction what) {
	// acquire the lock
	std::lock_guard<std::mutex> lock(buffer_mutex);
	
	// extract the informations
    dataType ris = (this->*extractors[static_cast<uint8_t>(what)])();
    return ris;
}


template<typename dataType>
dataType DataBuffer<dataType>::getAverage() {

   

    // check if the buffer is empty
    if (data_buffer.empty()) {
        return 0;
    }

    // check if we need to recompute the summ
    if (average_counter < RESET_COUNTER) {

        summ = 0;

        for(typename std::list<dataType>::iterator element = data_buffer.begin(); element != data_buffer.end(); ++element) {
            summ += *element;
        }
    }


    dataType average = summ / static_cast<dataType>(data_buffer.size());


    return average;
}




template<typename dataType>
dataType DataBuffer<dataType>::getVariance() {

    // check if the buffer has at least 2 elements
    if (data_buffer.size() < 2) {
        return 0;
    }

    // check if we really need to compute the variance again
    if (variance_available) {
        return variance;
    }

    // otherwise compute the average
    dataType average = getAverage();

    // compute the variance
    variance = 0;
    for(typename std::list<dataType>::iterator element = data_buffer.begin(); element != data_buffer.end(); ++element) {

        dataType first = std::max((*element), average);
        dataType second = std::min((*element), average);

        dataType diff = first - second;
        variance += diff*diff;
    }
    variance /= static_cast<dataType>(data_buffer.size() - 1);

    // set the flag
    variance_available = true;

    return variance;
}





template<typename dataType>
dataType DataBuffer<dataType>::getMin() {

    if (min_available) {
        return min;
    }

    if (data_buffer.empty()) {
        return 0;
    }

    dataType m = data_buffer.front();
    for(typename std::list<dataType>::iterator e = data_buffer.begin(); e != data_buffer.end(); ++e) {
        if (*e < m) {
            m = *e;
        }
    }

    return m;
}





template<typename dataType>
dataType DataBuffer<dataType>::getMax() {


    if (max_available) {
        return max;
    }

    if (data_buffer.empty()) {
        return 0;
    }

    dataType m = data_buffer.front();
    for(typename std::list<dataType>::iterator e = data_buffer.begin(); e != data_buffer.end(); ++e) {
        if (*e > m) {
            m = *e;
        }
    }

    return m;
}


template<typename dataType>
DataBuffer<dataType>::DataBuffer(const DataBuffer& data_buffer):extractors(  { __ARGO_EXTRACTORS }  ){

    for(typename std::list<dataType>::const_iterator data = data_buffer.data_buffer.cend(); data != data_buffer.data_buffer.cend(); ++data) {
        this->data_buffer.push_back(*data);
    }

    this->average_counter = data_buffer.average_counter;
    this->max = data_buffer.max;
    this->max_available = data_buffer.max_available;
    this->max_size = data_buffer.max_size;
    this->min_available = data_buffer.min_available;
    this->min = data_buffer.min;
    this->summ = data_buffer.summ;
    this->variance = data_buffer.variance;
    this->variance_available = data_buffer.variance_available;
}

template<typename dataType>
DataBuffer<dataType>::~DataBuffer() {
    data_buffer.clear();
}


} // namespace Monitor
} // namespace Argo


#endif //ARGO_MONITOR_DATA_BUFFER_H
