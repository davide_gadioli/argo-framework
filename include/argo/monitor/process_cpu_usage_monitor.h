/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_PROCESS_CPU_USAGE_MONITOR_H
#define ARGO_PROCESS_CPU_USAGE_MONITOR_H


#include <memory>
#include <chrono>
#include <ratio>

#include "argo/config.h"
#include "argo/monitor/data_buffer.h"
#include "argo/monitor/goal.h"



/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {
	
/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */	
namespace monitor {



#ifdef WITH_HARDWARE_COUNTER
// default SoftwareCounter
enum class CounterType: uint8_t {
    SoftwareCounter,
    HardwareCounter
};
#endif


/**
 * @brief  The Process CPU usage monitor
 *
 * @details
 * This class represent a monitor that observe the percentage of time that the application
 * has spent in user or system time over the observed period. This class use two methods
 * in order to collect the data. A Software Counter and an Hardware counter.
 * The former use the getrusage syscall, the latter use the  clock_gettime function.
 * The hardware counter retrieve a measure in nanoseconds, while the soft counter in milliseconds.
 * However if the process is migrated from one physical core to another, the value may be bogus.
 * 
 * @note
 * The hardware counter is available only in Linux environment, so on Mac OS X this counter is not
 * available 
 */
class ProcessCpuUsageMonitor: public DataBuffer<CpuUsage_t> {
public:
	
	/**
	 * @brief  Constructor with a Software Counter
	 * 
	 * @param window_size The dimension of the observation window
	 * 
	 */
    ProcessCpuUsageMonitor(WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);

#ifdef WITH_HARDWARE_COUNTER
	/**
	 * @brief  General constructor, available only in Linux
	 * 
	 * @param counter_type If the monitor use a Software or an Hardware counter
	 * 
	 * @param window_size The dimension of the observation window
	 * 
	 * @note
	 * For execution time below 100ms is better to use the Hardware counter
	 * 
	 */
    ProcessCpuUsageMonitor(CounterType counter_type, WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);
#endif


	/**
	 * @brief  Start the observation
	 * 
	 *  @note
	 * In an OpenCL context, the time spent building the kernel at Run-Time is counted.
	 */
    void start();

	/**
	 * @brief  Stop the observation and push the new data in the buffer
	 * 
	 */
    void stop();


private:

    // wall-time start point
    std::chrono::steady_clock::time_point tStart;

    // process usage start point with hardware counter
    timespec uStart;


    // flag for the measuring state
    bool started;


    // function pointer that compute the CPU time
    std::function<void(timespec&)> getProcessTime;
};






typedef std::shared_ptr<argo::monitor::ProcessCpuUsageMonitor> ProcessCpuUsageMonitorPtr;

typedef argo::monitor::Goal<argo::monitor::CpuUsage_t> ProcessCpuUsageGoal;
typedef std::shared_ptr<ProcessCpuUsageGoal> ProcessCpuUsageGoalPtr;


} // namespace monitor

} // namespace argo



#endif // ARGO_PROCESS_CPU_USAGE_MONITOR_H
