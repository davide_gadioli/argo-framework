/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/asrtm/asrtm.h"
#include "argo/asrtm/log.h"
#include <set>

using namespace std;


namespace argo { namespace asrtm {

   OPMetric_t Asrtm::getMetricValue(std::string metricName) {
       return currentManager->getMetricValue(metricName);
   }


 	Asrtm::Asrtm(const OperatingPointsList& opList) {

		// check the list is empty
		if  ( opList.empty() ) {
			LOG_NEW_LINE( "The Operating point list is empty!" )
			throw std::logic_error("Error: The operating list is empty!");
		}

 		LOG_NEW_LINE("Creating the asrtm")
 		ADD_INDENT()
 		LOG_NEW_LINE("Obtaining the states list")
 		ADD_INDENT()
 		// get the states' list 
 		set<string> states;
 		for(auto op_it = opList.begin(); op_it != opList.end(); ++op_it) {
 			for(auto state_it = op_it->states.begin(); state_it != op_it->states.end(); ++state_it) {
 				states.insert(*state_it);
 			}
 		}
 		REMOVE_INDENT()
 		LOG_NEW_LINE("Found " + std::to_string(states.size()) + " states!")

 		// create the managers
 		LOG_NEW_LINE("Building the states")
 		ADD_INDENT()
 		for(auto state_it = states.begin(); state_it != states.end(); ++state_it) {

 			LOG_NEW_LINE("Build the state " + *state_it)
 			ADD_INDENT()

 			// build its op list
 			OperatingPointsList list;
 			for(auto op_it = opList.begin(); op_it != opList.end(); ++op_it) {
 				// check if the op belong to this state
 				bool found = false;
 				for(auto it = op_it->states.begin(); it != op_it->states.end(); ++it) {
 					if ((*it).compare(*state_it) == 0) {
 						found = true;
 						break;
 					}
 				}
 				if (found) {
                    list.push_back(OperatingPoint(*op_it));
 				}
 			}
 			LOG_NEW_LINE("This state has " + std::to_string(list.size()) + " operating points")

 			// build the manager
 			OPManagerPtr manager(new OPManager(list));

 			// insert the manager
 			managers.insert(make_pair(*state_it, manager));
 			REMOVE_INDENT()
 			LOG_NEW_LINE("State built")
 		}
 		REMOVE_INDENT()
 		LOG_NEW_LINE("States created")

 		// set the first manager as the current one
 		currentManager = managers.begin()->second;

 		REMOVE_INDENT()
 		LOG_NEW_LINE("Asrtm created")
 	}


 	void Asrtm::changeState(std::string newState) {
 		// get the state
 		auto state_it = managers.find(newState);

 		// check if it's valid
 		if (state_it == managers.end()) {
 			throw std::logic_error("Error: The state " + newState + " doesn't exists!");
 		}

 		// change the state
 		currentManager = state_it->second;

 		// eventually reset the windows
 		currentManager->resetWindows();
 	}


    Asrtm::Asrtm(const Asrtm &asrtm) {

        this->currentManager = asrtm.currentManager;

        for(std::map<std::string, OPManagerPtr>::const_iterator manager = asrtm.managers.cbegin(); manager != asrtm.managers.cend(); ++manager) {
            this->managers.insert(std::pair<std::string, OPManagerPtr>(manager->first, manager->second));
        }
    }

    Asrtm::~Asrtm() {
        managers.clear();
    }


    ApplicationParameters Asrtm::getBestApplicationParameters(bool *changed) {
        return currentManager->getBestApplicationParameters(changed);
 	}

    void Asrtm::addStaticConstraintOnBottom(std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
 		currentManager->addStaticConstraintOnBottom(metricName, cFun, value);
 	}

    void Asrtm::addStaticConstraintOnTop(std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
 		currentManager->addStaticConstraintOnTop(metricName, cFun, value);
 	}

    void Asrtm::addStaticConstraintBefore(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
 		currentManager->addStaticConstraintBefore(position, metricName, cFun, value);
 	}

    void Asrtm::addStaticConstraintAfter(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
 		currentManager->addStaticConstraintAfter(position, metricName, cFun, value);
 	}

 	void Asrtm::removeConstraintOnBottom() {
 		currentManager->removeConstraintOnBottom();
 	}

 	void Asrtm::removeConstraintOnTop() {
 		currentManager->removeConstraintOnTop();
 	}

    void Asrtm::removeConstraintAt(ConstraintPosition_t position) {
 		currentManager->removeConstraintAt(position);
 	}

    void Asrtm::addRelaxableConstraint(std::string metricName, ComparisonFunction cFun, OPMetric_t value, std::function<void()> callbackFunction) {
 		currentManager->addRelaxableConstraint(metricName, cFun, value, callbackFunction);
 	}

    void Asrtm::changeStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t newValue) {
 		currentManager->changeStaticConstraintGoalValue(position, newValue);
 	}

    void Asrtm::incrementStaticConstraintGoalValue(ConstraintPosition_t position, float incPerc) {
 		currentManager->incrementStaticConstraintGoalValue(position, incPerc);
 	}

    void Asrtm::setGeometricRank(std::map<std::string, float> structure, RankObjective objective) {
 		currentManager->setGeometricRank(structure, objective);
 	}

    void Asrtm::setLinearRank(std::map<string, float> structure, RankObjective objective) {
 		currentManager->setLinearRank(structure, objective);
 	}

 	void Asrtm::setSimpleRank(std::string metricName, RankObjective objective) {
 		currentManager->setSimpleRank(metricName, objective);
 	}

    bool Asrtm::isPossible() {
        return currentManager->isPossible();
    }

    double Asrtm::getStaticConstraintValue(ConstraintPosition_t position) {
        return currentManager->getStaticConstraintGoalValue(position);
    }
} // namespace asrtm

} // namespace argo
