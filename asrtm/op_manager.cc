/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "argo/asrtm/log.h"
#include "argo/asrtm/op_manager.h"


 /** Logging macro (used only inside the op manager) **/
#if LOG_ENABLED > 0
 #include <string>
 #include <algorithm>
 #define LOG_CONSTRAINT() do {\
   for( auto it = constraints.begin(); it != constraints.end(); ++it) {\
    LOG_NEW_LINE("Constraint on '" + (*it)->getName() + "'")\
   }\
   } while(0);                                                                    
#else
 #define LOG_CONSTRAINT()
#endif

namespace argo { namespace asrtm {



OPMetric_t OPManager::getMetricValue(std::string metricName) {
    return opList[currentBestOP].metrics[metricName];
}



  
OPManager::OPManager(OperatingPointsList opList) : opList(opList) {

  if (unlikely( opList.empty())) {
    throw std::logic_error("Error: The operating list is empty!");
  }

  NEW_LINE()
  
  // create the model
  std::shared_ptr<Model> model(new Model(this->opList.size()));
  this->model = model;

  LOG_NEW_LINE("Creating an op manager")
  ADD_INDENT()

  LOG_NEW_LINE("Operating point's number: " + std::to_string(this->opList.size()))
 
  // populate the model and store the parameters
  OperatingPointID_t id = 0;
  for(std::vector<OperatingPoint>::iterator it = this->opList.begin(); it != this->opList.end(); ++it) {
    it->id = id;
    model->addOperatingPointToS1(id);
    appParams.push_back(it->parameters);
    id++;
  }
  

  LOG_NEW_LINE("Starting model:")
  ADD_INDENT()
  PRINT_MODEL(this->model)
  REMOVE_INDENT()

  // with no constraint nor ranks, all the ops are the same, pick the first
  currentBestOP = model->S1.front();

  // init the unsatisfiable constraint as the end of the constraints
  unsatisfiableConstraint = constraints.end();

  // init the ranks vector
  ranks.resize(opList.size(), 0);

  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  REMOVE_INDENT()
  LOG_NEW_LINE("Op manager created")
  NEW_LINE()
}


OPManager::OPManager(const OPManager &manager) {

    // copy the simple attribute
    this->model = manager.model;
    this->constraints = manager.constraints;
    this->currentBestOP = manager.currentBestOP;
    this->unsatisfiableConstraint = manager.unsatisfiableConstraint;


    // deep-copy the vectors
    for(std::vector<ApplicationParameters>::const_iterator param = manager.appParams.cbegin(); param != manager.appParams.cend(); ++param) {
        this->appParams.push_back(*param);
    }

    for(std::vector<Rank_t>::const_iterator rank = manager.ranks.cbegin(); rank != manager.ranks.cend(); ++rank) {
        this->ranks.push_back(*rank);
    }

    for(OperatingPointsList::const_iterator op = manager.opList.cbegin(); op != manager.opList.cend(); ++op) {
        this->opList.push_back(*op);
    }
}

OPManager::~OPManager() {
    appParams.clear();
    ranks.clear();
    opList.clear();
}





void OPManager::changeStaticConstraintGoalValue(OperatingPointID_t position, OPMetric_t newValue) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  // find the constraint
  auto it = constraints.begin();
  for(OperatingPointID_t index = 0; index < position; ++index, ++it) {
    if (it == constraints.end()) {
      break;
    }
  }

  // get the constraint
  KnownCharacteristicPtr constraint = std::dynamic_pointer_cast<KnownCharacteristic>(*it);

  // check if it's a valid cast
  if (!constraint) {
    throw std::runtime_error("Error: The " + std::to_string(position + 1) + "° constraint is not static!");
  }

  // otherwise change the goal value
  constraint->changeGoalValue(newValue);
}

void OPManager::incrementStaticConstraintGoalValue(OperatingPointID_t position, OPMetric_t incPerc) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  // find the constraint
  auto it = constraints.begin();
  for(OperatingPointID_t index = 0; index < position; ++index, ++it) {
    if (it == constraints.end()) {
      break;
    }
  }

  // get the constraint
  KnownCharacteristicPtr constraint = std::dynamic_pointer_cast<KnownCharacteristic>(*it);

  // check if it's a valid cast
  if (!constraint) {
    throw std::runtime_error("Error: The " + std::to_string(position + 1) + "° constraint is not static!");
  }

  // otherwise change the goal value
  constraint->incrementGoal(incPerc);
}

double OPManager::getStaticConstraintGoalValue(OperatingPointID_t position) {
    // lock over the constraint
    std::lock_guard<std::mutex> lg(constraintMutex);

    // find the constraint
    auto it = constraints.begin();
    for(OperatingPointID_t index = 0; index < position; ++index, ++it) {
      if (it == constraints.end()) {
        break;
      }
    }

    // get the constraint
    KnownCharacteristicPtr constraint = std::dynamic_pointer_cast<KnownCharacteristic>(*it);

    // check if it's a valid cast
    if (!constraint) {
      throw std::runtime_error("Error: The " + std::to_string(position + 1) + "° constraint is not static!");
    }

    // otherwise return the goal value
    return constraint->getGoalValue();
}


void OPManager::setGeometricRank(std::map<std::string, OPMetric_t> structure, RankObjective objective) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  NEW_LINE()
  LOG_NEW_LINE("Setting the geometric rank")
  ADD_INDENT()

  // check the rank direction
  int maxminCoef = -1;
  if (objective == RankObjective::Maximize) {
    maxminCoef = 1;
  }

  // compose the rank
  OperatingPointID_t index = 0;
  for(auto op_it = opList.begin(); op_it != opList.end(); ++op_it, ++index) {
    // reset the rank to the null value
    ranks[index] = 1;
    for (auto rank_it = structure.begin(); rank_it != structure.end(); ++rank_it ) {
      // check the metric/param value
      auto metricIterator = op_it->metrics.find(rank_it->first);
      if (metricIterator != op_it->metrics.end()) {
        ranks[index] *= pow(metricIterator->second, rank_it->second);
      } else {
        auto paramIterator = op_it->parameters.find(rank_it->first);
        if (paramIterator != op_it->parameters.end()) {
          ranks[index] *= pow(paramIterator->second, rank_it->second);
        } else {
          throw std::logic_error("Error: Unable to find a metric or parameter named " + rank_it->first);
        }
      }
    }
    ranks[index] *= maxminCoef;
  }

  // declare the proposed op
  OperatingPointID_t proposedBestOP;
  
  // get the new best op
  if (!model->S1.empty()) {
    proposedBestOP = getBestOPFromModel();    
  } else {
    proposedBestOP = getBestOPFromUnsatisfiableConstraint();
  }

  // check if the new best op is actually different from the current
  if (currentBestOP != proposedBestOP) {
    resetWindows();
  }

  // set the current op
  currentBestOP = proposedBestOP;
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  REMOVE_INDENT()
  LOG_NEW_LINE("Rank setted")
  NEW_LINE()
}


void OPManager::setLinearRank(std::map<std::string, OPMetric_t> structure, RankObjective objective) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  NEW_LINE()
  LOG_NEW_LINE("Setting the linear rank")
  ADD_INDENT()

  // check the rank direction
  int maxminCoef = -1;
  if (objective == RankObjective::Maximize) {
    maxminCoef = 1;
  }

  // compose the rank
  OperatingPointID_t index = 0;
  for(auto op_it = opList.begin(); op_it != opList.end(); ++op_it, ++index) {
    // reset the rank to the null value
    ranks[index] = 0;
    for (auto rank_it = structure.begin(); rank_it != structure.end(); ++rank_it ) {
      // check the metric/param value
      auto metricIterator = op_it->metrics.find(rank_it->first);
      if (metricIterator != op_it->metrics.end()) {
        ranks[index] += metricIterator->second * rank_it->second;
      } else {
        auto paramIterator = op_it->parameters.find(rank_it->first);
        if (paramIterator != op_it->parameters.end()) {
          ranks[index] += paramIterator->second * rank_it->second;
        } else {
          throw std::logic_error("Error: Unable to find a metric or parameter named " + rank_it->first);
        }
      }
    }
    ranks[index] *= maxminCoef;
  }

  // declare the proposed op
  OperatingPointID_t proposedBestOP;
  
  // get the new best op
  if (!model->S1.empty()) {
    proposedBestOP = getBestOPFromModel();    
  } else {
    proposedBestOP = getBestOPFromUnsatisfiableConstraint();
  }

  // check if the new best op is actually different from the current
  if (currentBestOP != proposedBestOP) {
    resetWindows();
  }

  // set the current op
  currentBestOP = proposedBestOP;
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  REMOVE_INDENT()
  LOG_NEW_LINE("Rank setted")
  NEW_LINE()
}



void OPManager::setSimpleRank(std::string metricName, RankObjective objective) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  NEW_LINE()
  LOG_NEW_LINE("Setting the simple rank")
  ADD_INDENT()

  // check the rank direction
  int maxminCoef = -1;
  if (objective == RankObjective::Maximize) {
    maxminCoef = 1;
  }

  // compose the rank
  OperatingPointID_t index = 0;
  for(auto op_it = opList.begin(); op_it != opList.end(); ++op_it, ++index) {
    // reset the rank to the null value
    ranks[index] = 0;
    auto metricIterator = op_it->metrics.find(metricName);
    if (metricIterator != op_it->metrics.end()) {
      ranks[index] = metricIterator->second;
    } else {
      auto paramIterator = op_it->parameters.find(metricName);
      if (paramIterator != op_it->parameters.end()) {
        ranks[index] = paramIterator->second;
      } else {
        throw std::logic_error("Error: Unable to find a metric or parameter named " + metricName);
      }
    }
    ranks[index] *= maxminCoef;
  }

  // declare the proposed op
  OperatingPointID_t proposedBestOP;
  
  // get the new best op
  if (!model->S1.empty()) {
    proposedBestOP = getBestOPFromModel();    
  } else {
    proposedBestOP = getBestOPFromUnsatisfiableConstraint();
  }

  // check if the new best op is actually different from the current
  if (currentBestOP != proposedBestOP) {
    resetWindows();
  }

  // set the current op
  currentBestOP = proposedBestOP;
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  REMOVE_INDENT()
  LOG_NEW_LINE("Rank setted")
  NEW_LINE()
}


void OPManager::addConstraintOnBottom(ConstraintPtr constraint, argo::DataFunction dFun) {
  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  

  NEW_LINE()
  LOG_NEW_LINE("Adding a constraint on bottom")
  ADD_INDENT()
  ConstraintList::iterator position = constraints.end();
  addConstraint(constraint, position, dFun);
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::addConstraintOnTop(ConstraintPtr constraint, argo::DataFunction dFun) {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);

  NEW_LINE()
  LOG_NEW_LINE("Adding a constraint on top")
  ADD_INDENT()
  ConstraintList::iterator position = constraints.begin();
  addConstraint(constraint, position, dFun);
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::addConstraintBefore(OperatingPointID_t position, ConstraintPtr constraint, argo::DataFunction dFun) {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  
  NEW_LINE()
  LOG_NEW_LINE("Adding a constraint before the " + std::to_string(position + 1) + "° constraint")
  ADD_INDENT()
  ConstraintList::iterator it_position = constraints.begin();

  for(OperatingPointID_t counter=0; counter < position; counter++) {
    if (it_position != constraints.end()) {
      it_position++;
    } else {
      break;
    }
  }
  
  addConstraint(constraint, it_position, dFun);
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::addConstraintAfter(OperatingPointID_t position, ConstraintPtr constraint, argo::DataFunction dFun) {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  

  NEW_LINE()
  LOG_NEW_LINE("Adding a constraint after the " + std::to_string(position + 1) + "° constraint")
  ADD_INDENT()
  ConstraintList::iterator it_position = constraints.begin();

  for(OperatingPointID_t counter=0; counter < position; counter++) {
    if (it_position != constraints.end()) {
      it_position++;
    } else {
      break;
    }
  }

  // because the actual insertion is before
  if (it_position != constraints.end()) {
    it_position++;
  }
  
  addConstraint(constraint, it_position, dFun);
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::removeConstraintOnBottom() {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  

  NEW_LINE()
  LOG_NEW_LINE("Remove a constraint on bottom")
  ADD_INDENT()
  if (!constraints.empty()) {
    ConstraintList::iterator position = constraints.end();
    --position;
    removeConstraint(position);
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::removeConstraintOnTop() {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  

  NEW_LINE()
  LOG_NEW_LINE("Remove a constraint on top")
  ADD_INDENT()
  if (!constraints.empty()) {
    ConstraintList::iterator position = constraints.begin();
    removeConstraint(position);
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::removeConstraintAt(uint32_t position) {

  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);
  

  NEW_LINE()
  LOG_NEW_LINE("Adding a constraint after the " + std::to_string(position) + "° constraint")
  ADD_INDENT()
  ConstraintList::iterator it_position = constraints.begin();

  for(OperatingPointID_t counter=0; counter < position; counter++) {
    if (it_position != constraints.end()) {
      ++it_position;
    } else {
      --it_position;
      break;
    }
  }
  
  if (!constraints.empty()) {
     removeConstraint(it_position);
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("Constraint added")
  NEW_LINE()
}

void OPManager::addStaticConstraintOnBottom(std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
  

  // create the KnownCharacteristic
  std::shared_ptr<KnownCharacteristic> kc(new KnownCharacteristic(metricName, opList, cFun, value));

  // add the constraint
  addConstraintOnBottom(std::dynamic_pointer_cast<Constraint>(kc));
}

void OPManager::addStaticConstraintOnTop(std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
  

  // create the KnownCharacteristic
  std::shared_ptr<KnownCharacteristic> kc(new KnownCharacteristic(metricName, opList, cFun, value));

  // add the constraint
  addConstraintOnTop(std::dynamic_pointer_cast<Constraint>(kc));
}

void OPManager::addStaticConstraintBefore(uint32_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
  

  // create the KnownCharacteristic
  std::shared_ptr<KnownCharacteristic> kc(new KnownCharacteristic(metricName, opList, cFun, value));

  // add the constraint
  addConstraintBefore(position, std::dynamic_pointer_cast<Constraint>(kc));
}

void OPManager::addStaticConstraintAfter(uint32_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value) {
  

  // create the KnownCharacteristic
  std::shared_ptr<KnownCharacteristic> kc(new KnownCharacteristic(metricName, opList, cFun, value));

  // add the constraint
  addConstraintAfter(position, std::dynamic_pointer_cast<Constraint>(kc));
}

void OPManager::addRelaxableConstraint(std::string metricName, ComparisonFunction cFun, OPMetric_t value, std::function<void()> callbackFunction) {
  // lock over the constriant
  std::lock_guard<std::mutex> lg(constraintMutex);

  // create the KnownCharacteristic
  std::shared_ptr<KnownCharacteristic> kc(new KnownCharacteristic(metricName, opList, cFun, value, true));
  kc->setRelaxable(callbackFunction);

  // add the constraint
  addRelaxableConstraint(std::dynamic_pointer_cast<Constraint>(kc));
}

void OPManager::addConstraint(ConstraintPtr constraint, ConstraintList::iterator position, argo::DataFunction dFun) {
  
  LOG_NEW_LINE("initial model:")
  ADD_INDENT()
  PRINT_MODEL(this->model)
  REMOVE_INDENT()
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))

  // check if the model is unsatisfiable
  bool unsatisfiable = false;
  if (model->S1.empty()) {
    unsatisfiable = true;
  }
  
  // actually add the constraint
  constraints.insert(position, constraint);
   
  // if possible check if something changes
  OperatingPointID_t proposedBestOP = currentBestOP;
  bool found = false;
  
  LOG_NEW_LINE("check the ops in S1 if they are still valid")
  ADD_INDENT()
  // for every op in S1
  for( std::list<OperatingPointID_t>::iterator it = model->S1.begin(); it != model->S1.end(); /*   handled by the model  */) {

    // check if the op is valid
    if (!constraint->isAdmissible(*it)) {

      LOG_NEW_LINE("Removed op " + std::to_string(*it))
      // remove it
      it = model->removeOperatingPointFromS1(*it);

    } else {
      
      // if valid check it's better of the previous
      if (!found) {
	      proposedBestOP = *it;
	      found = true;
      } else {
        proposedBestOP = getBest(*it, proposedBestOP);
      }
      
      // eventually increment the iterator
      ++it;
    }
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("check done")
  
  // check if eventually the model is satisfable
  if (!found) {
    LOG_NEW_LINE("The model is unsatisfiable!")
    
    // check if it was unsatisfiable
    if (!unsatisfiable) {
      unsatisfiableConstraint = constraints.end();
      unsatisfiableConstraint--;
    }
    
    ADD_INDENT()
    proposedBestOP = getBestOPFromUnsatisfiableConstraint();
    REMOVE_INDENT()
  }
  
  // check if a new best op is necessary
  if (proposedBestOP != currentBestOP) {
    resetWindows();
    currentBestOP = proposedBestOP;
  }
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  LOG_NEW_LINE("New list of constraint: ")
  ADD_INDENT()
  LOG_CONSTRAINT()
  REMOVE_INDENT()
}

void OPManager::removeConstraint(ConstraintList::iterator position) {
  
  LOG_NEW_LINE("initial model:")
  ADD_INDENT()
  PRINT_MODEL(this->model)
  REMOVE_INDENT()
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))

  LOG_NEW_LINE("trying to remove constraint on " + (*position)->getName())

  // check if the model is unsatisfiable
  bool unsatisfiable = false;
  if (model->S1.empty()) {
    unsatisfiable = true;
  }

  // get the constraint
  ConstraintPtr erasedConstraint = *position;

  // check if we are attempiting to remove the unsatisfiable constraint
  if (unsatisfiableConstraint == position) {
    if (unsatisfiableConstraint != constraints.begin()) {
      --unsatisfiableConstraint;
    }
  }

  // actually remove the constraint
  constraints.erase(position);


  // check if it's needed to add operating points
  LOG_NEW_LINE("check the ops in [LimitOP, WorstOP] are valid")
  ADD_INDENT()
  bool found_strictly = false;
  bool found_loosely = false;
  OperatingPointID_t proposedBestOP = currentBestOP;
  auto it = erasedConstraint->getLimitOP();
  if (erasedConstraint->isAdmissible(*it)) {
    ++it;
  }
  auto end = erasedConstraint->getWorstOP();
  ++end;
  for(/* already handled */; it != end; it++) {
    INDENT()
    LOG("check the op " + std::to_string(*it) + ", ")
    if (isValid(*it, CheckType::Strictly)) {
      LOG("it's strictly valid, ")
      if (found_strictly) {
        proposedBestOP = getBest(*it, proposedBestOP);
        LOG("and the best is " + std::to_string(proposedBestOP))
      } else {
        LOG("and it's the fisrt")
        proposedBestOP = *it;
        found_strictly = true;
      }
      model->addOperatingPointToS1(*it);
    } else {
      LOG("it's not strictly valid")
      if (!found_strictly && unsatisfiable && isValid(*it, CheckType::Loosely)) {
        LOG(", but is loosely valid, ")
        if (found_loosely) {
          proposedBestOP = getBest(*it, proposedBestOP);
          LOG("and the best is " + std::to_string(proposedBestOP))
        } else {
          LOG("and it's the first")
          proposedBestOP = *it;
          found_loosely = true;
        }
      }
    }
    NEW_LINE()
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("check done")

  // eventually check if something change
  if (found_strictly) {
    if (!unsatisfiable) {
      proposedBestOP = getBest(proposedBestOP, currentBestOP);
    } else {
      unsatisfiableConstraint = constraints.end();
    }
  } else {
    proposedBestOP = getBest(proposedBestOP, currentBestOP);
  }
  
  // check if a new best op is necessary
  if (proposedBestOP != currentBestOP) {
    resetWindows();
    currentBestOP = proposedBestOP;
  }
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  LOG_NEW_LINE("New list of constraint: ")
  ADD_INDENT()
  LOG_CONSTRAINT()
  REMOVE_INDENT()
}

void OPManager::addRelaxableConstraint(ConstraintPtr constraint) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  relaxableConstraints.push_back(constraint);
}

ApplicationParameters OPManager::getBestApplicationParameters(bool *changed) {
  // lock over the constraint
  std::lock_guard<std::mutex> lg(constraintMutex);

  NEW_LINE()
  LOG_NEW_LINE("Getting the best application parameters")
  ADD_INDENT()
  LOG_NEW_LINE("initial model:")
  ADD_INDENT()
  PRINT_MODEL(this->model)
  REMOVE_INDENT()
  LOG_NEW_LINE("current op: " + std::to_string(currentBestOP))
  
  // check the relaxable constraints
  LOG_NEW_LINE("Checking relaxable constraints")
  ADD_INDENT()
  checkRelaxableConstraints();
  REMOVE_INDENT()
  LOG_NEW_LINE("Checked")
  
  // state if the current op si still valid
  bool change = false;
  
  // state if the model was unsatisfiable i.e. a constraints can't be satisfied
  bool satisfable = true;
  
  // state if the model become unsatisfiable from satisfable
  bool nowUnsatisfiable = false;
  
  // check if the model was satisfable
  if (model->S1.empty()) {
    satisfable = false;
    LOG_NEW_LINE("The model is unsatisfiable, blaming '" + (*unsatisfiableConstraint)->getName() + "'")
  }

  // ************************************************** get the new information
  // for each constraint
  // bool validConstraint = true;
  LOG_NEW_LINE("Checking if the constraints need to update the model")
  ADD_INDENT()
  for(auto it = constraints.begin(); it != constraints.end(); ++it) {

    LOG_NEW_LINE("updating a constraint")
    ADD_INDENT()
    
    // get the information from the model
    bool change_proposed = (*it)->updateModel(model, currentBestOP);

    // check if the constraint is meaningfull
    //if (validConstraint) {
      change = change || change_proposed;
    //}

    // check if the constraint is meaningfull
//    if (unsatisfiableConstraint == it) {
//      LOG_NEW_LINE("this is the unsatisfiable constraint")
//      validConstraint = false;
//    }

    REMOVE_INDENT()
    LOG_NEW_LINE("constraint updated")
    
    // if the S1 was not empty in the beginning and
    // if S1 is empty now, it means that this constraint is unsatisfiable
    if (model->S1.empty() && satisfable && !nowUnsatisfiable) {
      unsatisfiableConstraint = it;
      nowUnsatisfiable = true;
      LOG_NEW_LINE("the model has become unsatisfiable, blame the constraint on '" + (*unsatisfiableConstraint)->getName() + "'")
    }
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("model updated")
  LOG_NEW_LINE("new model:")
  ADD_INDENT()
  PRINT_MODEL(this->model)
  REMOVE_INDENT()
  
  // ************************************************** check if we can add some ops
  
  // information about the new better op totally valid
  OperatingPointID_t new_optimal_OP_stryctly_valid = 0;
  bool found_strictly = false;
  
  // information about the new better op semi valid
  OperatingPointID_t new_optimal_OP_loosely_valid = 0;
  bool found_loosely = false;

  // for each op in S2
  LOG_NEW_LINE("validating the ops in S2:")
  ADD_INDENT()
  for(auto it = model->S2.begin(); it != model->S2.end(); /* Handled by the model */ ) {
    INDENT()  
    LOG("check op " + std::to_string(*it) + ", ")
    // check if it's valid
    if (isValid(*it, CheckType::Strictly)) {
      LOG("it's strictly valid, ")
      
      // check if it's the first
      if (!found_strictly) {
        // save the informations
        new_optimal_OP_stryctly_valid = *it;
        found_strictly = true;
        LOG("and it's the first strictly valid")
      } else {
        // it's not the first, so check if this is better of the previous	
        new_optimal_OP_stryctly_valid = getBest(*it, new_optimal_OP_stryctly_valid);
        LOG("and the best added yet is " + std::to_string(new_optimal_OP_stryctly_valid))
      }
      NEW_LINE()

      // commit the op in S1 because is a valid op
      it = model->commitOP(*it);
      
    } else {
      LOG("it's NOT strictly valid")
      
      // if the model is not satisfable and no strictly valid op are found
      if ((!satisfable) && (!found_strictly)) {
	
	      // check if the the op is valid for the upper constraints
	      if (isValid(*it, CheckType::Loosely)) {
	  
	        // get the best op that satisfy the upper constraint
          if (!found_loosely) {
	          new_optimal_OP_loosely_valid = *it;
	          found_loosely = true;
            LOG(", but the model was unsatisfable and it's the first loosely valid")
	        } else {
	          new_optimal_OP_loosely_valid = getBest(*it, new_optimal_OP_loosely_valid);
            LOG(", but the model was unsatisfable and the best added yet is " + std::to_string(new_optimal_OP_loosely_valid))
	        }
          } else {
              LOG(", and neither loosly.");
          }
      }
      
      NEW_LINE()
      // remove the op from S2 because is not strictly valid
      it = model->removeOperatingPointFromS2(*it);
    }
  }
  REMOVE_INDENT()
  LOG_NEW_LINE("validation done")
  
  //***************************************************** handle the changes
  
  OperatingPointID_t proposedBestOP = currentBestOP;
  
  // if the model was satisfable
  if ((satisfable) && (!nowUnsatisfiable)) {
    
    // if found added an op
    if (found_strictly) {
      
      // take the best op
      proposedBestOP = getBest(new_optimal_OP_stryctly_valid, currentBestOP);
      LOG_NEW_LINE("the best op which the current and the added one is: " + std::to_string(proposedBestOP))     
    } 
    
    // if the current op is removed and no better op are added
    // we must search the best from the model
    if ((change) && (proposedBestOP == currentBestOP)) {
      LOG_NEW_LINE("The current op is no more valid, and no better op is added, we must find another within S1")
      ADD_INDENT()
      proposedBestOP = getBestOPFromModel();
      REMOVE_INDENT()
    }
    
  } else {    // if the model was not satisfable
    
    // check if we found a strictly valid op
    if (found_strictly) {
      
      // set the current operating point
      proposedBestOP = new_optimal_OP_stryctly_valid;

      // set the unsatisfiable constraint as the end of the constriant list
      unsatisfiableConstraint = constraints.end();
      LOG_NEW_LINE("the model was unsatisfiable, but we found a strictly valid op: " + std::to_string(proposedBestOP))
      
    } else {
      
      // check if added a new op in an unsatisfiable model
      if ((found_loosely) && (!satisfable)) {
        proposedBestOP = getBest(new_optimal_OP_loosely_valid, currentBestOP);
        LOG_NEW_LINE("the model is unsatisfiable, and the best op which the current and the added one is: " + std::to_string(proposedBestOP))
      } 
	
      // if the current op is removed and no better op are added
      // we must search the best from the unsatisfiable constraint
      if ((change) && (proposedBestOP == currentBestOP)) {
        LOG_NEW_LINE("the model is unsatisfiable, the current op is no more valid and no better op is added")
        LOG_NEW_LINE("getting the best op from an unsatisfiable constraint")
        ADD_INDENT()
        proposedBestOP = getBestOPFromUnsatisfiableConstraint();
        REMOVE_INDENT()
        LOG_NEW_LINE("op obtained")
      }
    }
  }
  
  // check if the op is changed
  if (proposedBestOP != currentBestOP) {
    if (changed != nullptr) {
        *changed = true;
    }
    currentBestOP = proposedBestOP;
    LOG_NEW_LINE("new current op: " + std::to_string(currentBestOP))
    resetWindows();
  } else {
      if (changed != nullptr) {
          *changed = false;
      }
  }

  REMOVE_INDENT()
  LOG_NEW_LINE("Parameters obtained")
  // return it's application params
  return appParams[currentBestOP];
}


OperatingPointID_t OPManager::getBestOPFromModel() {
  LOG_NEW_LINE("searching the best op from S1")
  ADD_INDENT()
  
  // initialize the new bestOP
  OperatingPointID_t newOptimalOP = model->S1.front();
  
  // search if is present another optimal op
  for(auto it = model->S1.begin(); it != model->S1.end(); ++it) {
    newOptimalOP = getBest(*it, newOptimalOP);
    LOG_NEW_LINE("checking op " + std::to_string(*it) + ", the best is " + std::to_string(newOptimalOP))
  }

  REMOVE_INDENT()
  LOG_NEW_LINE("search done")
  // return the best op
  return newOptimalOP;
}


void OPManager::resetWindows() {
  LOG_NEW_LINE("clear windows data")
  // for every constraint
  for( auto it = constraints.begin(); it != constraints.end(); ++it) {
    (*it)->clearWindow();
  }
  
  // for every relaxable constraint
  for( auto it = relaxableConstraints.begin(); it != relaxableConstraints.end(); ++it) {
    (*it)->clearWindow();
  }
}

void OPManager::checkRelaxableConstraints() {
  for(auto it = relaxableConstraints.begin(); it != relaxableConstraints.end(); ++it) {
    (*it)->checkRelaxable(currentBestOP);
  }
}


bool OPManager::isPossible() {
    // lock over the constraint
    std::lock_guard<std::mutex> lg(constraintMutex);

    // check the model
    return !model->S1.empty();
}


OperatingPointID_t OPManager::getBestOPFromUnsatisfiableConstraint() {
  LOG_NEW_LINE("the unsatisfiable constraint is on metric '" + (*unsatisfiableConstraint)->getName() + "'")

  // first constraint check
  std::list<OperatingPointID_t>::iterator bestOP  = (*unsatisfiableConstraint)->getBestOP();
  if ((unsatisfiableConstraint == constraints.begin()) && (!(*unsatisfiableConstraint)->isAdmissible(*bestOP))) {
    LOG_NEW_LINE("The first constraint is not possible, check among the bests");
    ADD_INDENT();

    OperatingPointID_t worst_op_ins = *((*unsatisfiableConstraint)->getWorstOP());
    OperatingPointID_t current_best = *bestOP;

    // check if the oplist contain only one OP
    if ( current_best == worst_op_ins) {
        LOG_NEW_LINE("There is only one OP: " + std::to_string(*bestOP));
        REMOVE_INDENT();
        return current_best;
    }

    LOG_NEW_LINE("The best OP is: " + std::to_string(*bestOP) + ", check the next");
    // get the best op and the next
    std::list<OperatingPointID_t>::iterator next_bestOP = bestOP;
    ++next_bestOP;

    // check if there are more op with the same value, and return the BEST
    while((*unsatisfiableConstraint)->getDistance(*bestOP) == (*unsatisfiableConstraint)->getDistance(*next_bestOP)) {
        INDENT();
        LOG("The next op (" + std::to_string(*next_bestOP) + ") has the same difference");
        current_best = getBest(current_best, *next_bestOP);
        LOG(", the best is " + std::to_string(current_best));
        NEW_LINE();

        ++next_bestOP;
        if(unlikely(*next_bestOP == worst_op_ins)) {
            break;
        }
    }

    REMOVE_INDENT();
    LOG_NEW_LINE("The best OP is" + std::to_string(current_best));

    return current_best;
  }

  // get the limit op
  std::list<OperatingPointID_t>::iterator limitOP = (*unsatisfiableConstraint)->getLimitOP();

  // get the first op next to the limitOP (if the limit op is valid (special case))
  if ((*unsatisfiableConstraint)->isAdmissible(*limitOP)) {
    ++limitOP;
  }

  // init variable
  bool found = false;
  OperatingPointID_t newOP = 0;

  // search the best op
  LOG_NEW_LINE("Search for the best op loosely valid that satisfies the constraint too")
  ADD_INDENT()
  for (std::list<OperatingPointID_t>::iterator it = (*unsatisfiableConstraint)->getBestOP(); it != limitOP; ++it) {
    INDENT()
    LOG("check op " + std::to_string(*it))
    //check if the op is semi valid
    if (isValid(*it, CheckType::Loosely)) {
      if (!found) {
        newOP = *it;
        found = true;
      } else {
        newOP = getBest(*it, newOP);
      }
      LOG(", it's admissible, the best op yet is " + std::to_string(newOP))
    }
    NEW_LINE()
  }
  REMOVE_INDENT()

  // if found an op it's done
  if (found) {
    LOG_NEW_LINE("found the op " + std::to_string(newOP))
    return newOP;
  }

  // otherwise try the upper constraint
  LOG_NEW_LINE("no op found, try the upper constraint")
  --unsatisfiableConstraint;
  return getBestOPFromUnsatisfiableConstraint();
}
  
  
bool OPManager::isValid(OperatingPointID_t opID, CheckType type) {
  
  // init the result
  bool result = true;

  // set the final constraint
  ConstraintList::iterator limitConstraint;
  if (type == CheckType::Loosely) {
    limitConstraint = unsatisfiableConstraint;
    limitConstraint++;
  } else {
    limitConstraint = constraints.end();
  }
  
  // for all the constraints
  for( auto it = constraints.begin(); it != limitConstraint; ++it) {
    // check if admissible
    result = result && (*it)->isAdmissible(opID);
  }
  
  return result;
}


OperatingPointID_t OPManager::getBest(OperatingPointID_t opID1, OperatingPointID_t opID2) {


  // handle the satisfable case
  if (!model->S1.empty()) {
    if (ranks[opID1] > ranks[opID2]) {
      return opID1;
    } else {
      return opID2;
    }
  }

  // handle the unsatisfable case

  // for every lower constraint
  auto it = unsatisfiableConstraint;
  for(++it ; it != constraints.end(); ++it) {

    // check the validity of the ops
    bool op1valid = (*it)->isAdmissible(opID1);
    bool op2valid = (*it)->isAdmissible(opID2);

    // if only one is valid, it's the best op
    if (op1valid && !op2valid) {
      return opID1;
    }
    if (!op1valid && op2valid) {
      return opID2;
    }

    // if both are not valid, the best is the closest to the not achieved goal
    if (!op2valid && !op1valid) {
      double d1 = (*it)->getDistance(opID1);
      double d2 = (*it)->getDistance(opID2);

      if (d1 > d2) {
        return opID2;
      }

      if (d2 > d1) {
        return opID1;
      }

      // if they are at the same distance exit and handle it
      break;
    }

    // if both are valid, go to the next constraint
  }

  // find the distance respect the lower constraints
  float distance1 = 0;
  float distance2 = 0;
  for(++it; it != constraints.end(); ++it) {
      if (!(*it)->isAdmissible(opID1)) {
          distance1 += (*it)->getDistance(opID1);
      }

      if (!(*it)->isAdmissible(opID2)) {
          distance2 += (*it)->getDistance(opID2);
      }
  }

  // return the closer op wrt te lower constraints
  if (distance1 > distance2) {
      return opID2;
  }
  if (distance2 > distance1) {
      return opID1;
  }

  // if even these are equals, return the better
  if (ranks[opID1] > ranks[opID2]) {
    return opID1;
  } else {
    return opID2;
  }
}

  
  
  
  
} // namespace asrtm

} // namespace argo
