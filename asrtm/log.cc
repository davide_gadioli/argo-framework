/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 #include "argo/asrtm/log.h"
 #include <iostream>

 using namespace std;
 using namespace argo;


 void Log::addIndent() {
 	Log::indentLevel++;
 }

 void Log::removeIndent() {
 	Log::indentLevel--;
 }

 void Log::logNewLine(std::string what, const std::string function_name) {
	 
	 
	 
	 std::size_t found = function_name.find("updateModel");
	 if (found!=std::string::npos)
	 {
		 std::cout << "\033[1;31m";
	 }
	 else
	 {
		 found = function_name.find("getBest");
		 if (found!=std::string::npos)
		 {
			 std::cout << "\033[2;32m";
		 }
		 else
		 {
			 found = function_name.find("Predictor");
			 if (found!=std::string::npos)
				std::cout << "\033[2;35m"; 
		 }
	 }
	 
	 
	 
	 
	 
 	for (int i = 0; i < Log::indentLevel*Log::whiteSpaceTick; i++) {
 		cout << " ";
 	}
 	cout << what << endl;
	
	// Set the default color
	std::cout << "\033[0m";
 }

 void Log::log(std::string what, const std::string function_name) {
	 std::size_t found = function_name.find("updateModel");
	 if (found!=std::string::npos)
	 {
		 std::cout << "\033[1;31m";
	 }
	 else
	 {
		 found = function_name.find("getBest");
		 if (found!=std::string::npos)
		 {
			 std::cout << "\033[2;32m";
		 }
		 else
		 {
			 found = function_name.find("Predictor");
			 if (found!=std::string::npos)
				 std::cout << "\033[2;35m";
		 }
	 }
	 
	 cout << what;
	
	// Set the default color
	std::cout << "\033[0m";
 }

 void Log::indent() {
 	for (int i = 0; i < Log::indentLevel*Log::whiteSpaceTick; i++) {
 		cout << " ";
 	}
 }

 void Log::newLine() {
 	cout << endl;
 }


 uint8_t Log::indentLevel = 0;
