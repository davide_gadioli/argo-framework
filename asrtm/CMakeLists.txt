

###############################################
##             Build & install               ##
###############################################


# -------- Defining the source/header file

set( ASRTM_HDR_PATH ${ARGO_SOURCE_DIR}/include/argo/asrtm )

set( ASRTM_HDR_FILE     ${ASRTM_HDR_PATH}/operating_point.h
                        ${ASRTM_HDR_PATH}/asrtm_enums.h
                        ${ASRTM_HDR_PATH}/model.h
                        ${ASRTM_HDR_PATH}/constraint.h
                        ${ASRTM_HDR_PATH}/op_manager.h
                        ${ASRTM_HDR_PATH}/log.h
                        ${ASRTM_HDR_PATH}/known_characteristic.h
                        ${ASRTM_HDR_PATH}/observable_characteristic.h
                        ${ASRTM_HDR_PATH}/asrtm.h
                        )

set( ASRTM_SRC_FILE     model.cc
                        operating_point.cc
                        op_manager.cc
                        log.cc
                        known_characteristic.cc
                        asrtm.cc
                        )


# --------  Build & install the module
if( LIB_DYNAMIC )
        add_library( argo_asrtm SHARED ${ASRTM_SRC_FILE} ${ASRTM_HDR_FILE} )
        target_link_libraries( argo_asrtm argo_monitor)
        target_link_libraries( argo_asrtm rt )
        install( TARGETS argo_asrtm DESTINATION lib )
endif( LIB_DYNAMIC )

if( LIB_STATIC )
        add_library( argo_asrtmStatic STATIC ${ASRTM_SRC_FILE} ${ASRTM_HDR_FILE} )
        target_link_libraries( argo_asrtmStatic argo_monitorStatic)
        target_link_libraries( argo_asrtmStatic rt )
        install( TARGETS argo_asrtmStatic DESTINATION lib )
endif( LIB_STATIC )



# ------ Install the headers
install( FILES ${ASRTM_HDR_FILE} DESTINATION include/argo/asrtm )
