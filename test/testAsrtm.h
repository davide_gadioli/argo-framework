#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include "gtest/gtest.h"

#include "argo/asrtm/asrtm.h"
#include "argo/monitor/monitor.h"

TEST(Asrtm, noState) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        }
  };

  argo::asrtm::Asrtm manager(OPList);
}




TEST(Asrtm, oneState) {
	// define the op
	argo::asrtm::OperatingPointsList OPList = {
		{ // OP 1
      { // parameters
        { "param", 1 },
      },
      { // metrics
        { "something", 10 },
      },
      { // states
		  	"foo"
		  }
    }
	};

	argo::asrtm::Asrtm manager(OPList);
}


TEST(Asrtm, twoState) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parameters
        { "param", 1 },
      },
      { // metrics
        { "something", 10 },
      },
      { // states
        "foo"
      }
    },
    { // OP 2
      { // parameters
        { "param", 2 },
      },
      { // metrics
        { "something", 20 },
      },
      { // states
        "foo", "bar"
      }
    },
    { // OP 3
      { // parameters
        { "param", 3 },
      },
      { // metrics
        { "something", 30 },
      },
      { // states
        "bar"
      }
    },
  };

  argo::asrtm::Asrtm manager(OPList);
  manager.changeState("foo");
  manager.setSimpleRank("something", argo::RankObjective::Minimize);

  argo::asrtm::ApplicationParameters result = manager.getBestApplicationParameters();
  EXPECT_EQ(1 , result["param"]);

  manager.setSimpleRank("something", argo::RankObjective::Maximize);
  result = manager.getBestApplicationParameters();
  EXPECT_EQ(2 , result.find("param")->second);

  manager.changeState("bar");
  manager.setSimpleRank("something", argo::RankObjective::Minimize);
  result = manager.getBestApplicationParameters();
  EXPECT_EQ(2 , result.find("param")->second);

  manager.setSimpleRank("something", argo::RankObjective::Maximize);
  result = manager.getBestApplicationParameters();
  EXPECT_EQ(3 , result.find("param")->second);
}

TEST(Asrtm, testMacro) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parameters
        { "param", 1 },
      },
      { // metrics
        { "something", 10 },
      }
    },
    { // OP 2
      { // parameters
        { "param", 2 },
      },
      { // metrics
        { "something", 20 },
      }
    },
    { // OP 3
      { // parameters
        { "param", 3 },
      },
      { // metrics
        { "something", 30 },
      }
    }
  };


  argo::monitor::TimeMonitorPtr my_monitor( new argo::monitor::TimeMonitor());
  std::shared_ptr<argo::monitor::Goal<argo::monitor::Time_t>> goal1(new argo::monitor::Goal<argo::monitor::Time_t>(my_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 20));


  // create the asrtm
  argo::asrtm::Asrtm asrtm(OPList);

  // set rank and constraints
  asrtm.setSimpleRank("something", argo::RankObjective::Minimize);
  asrtm.addDynamicConstraintOnTop(goal1, "something");

  // get the parameters
  argo::asrtm::ApplicationParameters params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 3, params["param"]);




  // change goal
  goal1->setGoalValue(10);
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);



  // adding another constraint
  std::shared_ptr<argo::monitor::Goal<argo::monitor::Time_t>> goal2(new argo::monitor::Goal<argo::monitor::Time_t>(my_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));
  asrtm.addDynamicConstraintOnBottom(goal2, "something");
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);


  // changing the rank direction
  asrtm.setSimpleRank("something", argo::RankObjective::Maximize);
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);


  // remove a goal
  asrtm.removeConstraintOnBottom();
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 3, params["param"]);
}

TEST(Asrtm, testGeneric) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parameters
        { "param", 1 },
      },
      { // metrics
        { "something", 10 },
      }
    },
    { // OP 2
      { // parameters
        { "param", 2 },
      },
      { // metrics
        { "something", 20 },
      }
    },
    { // OP 3
      { // parameters
        { "param", 3 },
      },
      { // metrics
        { "something", 30 },
      }
    }
  };

  std::shared_ptr<argo::monitor::DataBuffer<int>> my_monitor(new argo::monitor::DataBuffer<int>(10));
  std::shared_ptr<argo::monitor::Goal<int>> goal1(new argo::monitor::Goal<int>(my_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 20));
  



  // create the asrtm
  argo::asrtm::Asrtm asrtm(OPList);

  // set rank and constraints
  asrtm.setSimpleRank("something", argo::RankObjective::Minimize);
  asrtm.addDynamicConstraintOnTop(goal1, "something");

  // get the parameters
  argo::asrtm::ApplicationParameters params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 3, params["param"]);




  // change goal
  goal1->setGoalValue(10);
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);



  // adding another constraint
  std::shared_ptr<argo::monitor::Goal<int>> goal2(new argo::monitor::Goal<int>(my_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));
  asrtm.addDynamicConstraintOnBottom(goal2, "something");
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);


  // changing the rank direction
  asrtm.setSimpleRank("something", argo::RankObjective::Maximize);
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 2, params["param"]);


  // remove a goal
  asrtm.removeConstraintOnBottom();
  params = asrtm.getBestApplicationParameters();
  ASSERT_EQ((argo::asrtm::OPParameter_t) 3, params["param"]);
}
