#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include "gtest/gtest.h"

#include "argo/monitor/goal.h"


TEST(GoalTest, handlingFunctions) {


    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(3));

    std::shared_ptr<argo::monitor::Goal<int>> g(new argo::monitor::Goal<int>(p, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 3));

    ASSERT_EQ(3, g->getGoalValue());

    g->setGoalValue(4);

    ASSERT_EQ(4, g->getGoalValue());

    g->incrementGoalValue(1.5);

    ASSERT_EQ(6, g->getGoalValue());

    g->incrementGoalValue(0.5);

    ASSERT_EQ(3, g->getGoalValue());

}


TEST(GoalTest, greaterInt) {

    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(3));

    std::shared_ptr<argo::monitor::Goal<int>> g(new argo::monitor::Goal<int>(p, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 3));

    ASSERT_FALSE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(1, g->getNAP());
    ASSERT_EQ(3, g->getRelativeError());
    ASSERT_EQ(3, g->getAbsoluteError());

    p->push(-3);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(-3, g->getObservedValue());
    ASSERT_EQ(6, g->getNAP());
    ASSERT_EQ(2, g->getRelativeError());
    ASSERT_EQ(6, g->getAbsoluteError());

    p->push(20);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(8, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());


    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());


}



TEST(GoalTest, greaterFloat) {

    std::shared_ptr<argo::monitor::DataBuffer<float>> p (new argo::monitor::DataBuffer<float>(3));

    std::shared_ptr<argo::monitor::Goal<float>> g(new argo::monitor::Goal<float>(p, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 3));

    ASSERT_FALSE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(1, g->getNAP());
    ASSERT_EQ(3, g->getRelativeError());
    ASSERT_EQ(3, g->getAbsoluteError());

    p->push(-3);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(-3, g->getObservedValue());
    ASSERT_EQ(6, g->getNAP());
    ASSERT_EQ(2, g->getRelativeError());
    ASSERT_EQ(6, g->getAbsoluteError());

    p->push(20);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(8.5, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());

    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());

}

TEST(GoalTest, greaterUnsigned) {

    std::shared_ptr<argo::monitor::DataBuffer<unsigned>> p (new argo::monitor::DataBuffer<unsigned>(3));

    std::shared_ptr<argo::monitor::Goal<unsigned>> g(new argo::monitor::Goal<unsigned>(p, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 3));

    ASSERT_FALSE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(1, g->getNAP());
    ASSERT_EQ(3, g->getRelativeError());
    ASSERT_EQ(3, g->getAbsoluteError());

    p->push(1);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(1, g->getObservedValue());
    ASSERT_EQ(0.5f, g->getNAP());
    ASSERT_EQ(2, g->getRelativeError());
    ASSERT_EQ(2, g->getAbsoluteError());

    p->push(20);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(10, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());


    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());

}


TEST(GoalTest, lessInt) {

    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(3));

    std::shared_ptr<argo::monitor::Goal<int>> g(new argo::monitor::Goal<int>(p, argo::DataFunction::Average, argo::ComparisonFunction::Less, -3));

    ASSERT_FALSE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(1, g->getNAP());
    ASSERT_EQ(3, g->getRelativeError());
    ASSERT_EQ(3, g->getAbsoluteError());

    p->push(3);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(3, g->getObservedValue());
    ASSERT_EQ(6, g->getNAP());
    ASSERT_EQ(2, g->getRelativeError());
    ASSERT_EQ(6, g->getAbsoluteError());

    p->push(-20);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(-8, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());


    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());


}



TEST(GoalTest, lessFloat) {

    std::shared_ptr<argo::monitor::DataBuffer<float>> p (new argo::monitor::DataBuffer<float>(3));

    std::shared_ptr<argo::monitor::Goal<float>> g(new argo::monitor::Goal<float>(p, argo::DataFunction::Average, argo::ComparisonFunction::Less, -3));

    ASSERT_FALSE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(1, g->getNAP());
    ASSERT_EQ(3, g->getRelativeError());
    ASSERT_EQ(3, g->getAbsoluteError());

    p->push(3);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(3, g->getObservedValue());
    ASSERT_EQ(6, g->getNAP());
    ASSERT_EQ(2, g->getRelativeError());
    ASSERT_EQ(6, g->getAbsoluteError());

    p->push(-20);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(-8.5, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());

    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());

}

TEST(GoalTest, lessUnsigned) {

    std::shared_ptr<argo::monitor::DataBuffer<unsigned>> p (new argo::monitor::DataBuffer<unsigned>(3));

    std::shared_ptr<argo::monitor::Goal<unsigned>> g(new argo::monitor::Goal<unsigned>(p, argo::DataFunction::Average, argo::ComparisonFunction::Less, 3));

    ASSERT_TRUE(g->check());
    ASSERT_EQ(0, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());

    p->push(1);

    ASSERT_TRUE(g->check());
    ASSERT_EQ(1, g->getObservedValue());
    ASSERT_EQ(0, g->getNAP());
    ASSERT_EQ(0, g->getRelativeError());
    ASSERT_EQ(0, g->getAbsoluteError());

    p->push(20);

    ASSERT_FALSE(g->check());
    ASSERT_EQ(10, g->getObservedValue());
    ASSERT_EQ(static_cast<float>(7)/13, g->getNAP());
    ASSERT_EQ(static_cast<float>(7)/10, g->getRelativeError());
    ASSERT_EQ(7, g->getAbsoluteError());


    g->setGoalValue(g->getObservedValue());

    ASSERT_FALSE(g->check());

}
