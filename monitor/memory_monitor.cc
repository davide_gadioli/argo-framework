/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <cstdio>
#include <cstring>
#include <stdexcept>


#include "argo/monitor/memory_monitor.h"


namespace argo {
namespace monitor {

MemoryMonitor::MemoryMonitor(WindowSize window_size):DataBuffer<Memory_t>(window_size) {}



void MemoryMonitor::extractMemoryUsage() {
    // get the monitor value
    Memory_t memory_usage_kb;
    std::shared_ptr<FILE> fp(fopen("/proc/self/statm", "r"), fclose);
    int result = ::fscanf(fp.get(), "%*d %lu", &memory_usage_kb);

    // check if it's correct
    if (result == EOF) {
        throw std::runtime_error("Error, can't get the memory measure");
    }

    // push the value
    push(memory_usage_kb);
}



Memory_t MemoryMonitor::extractVmPeakSize() {
	std::shared_ptr<FILE> fp(fopen("/proc/self/status", "r"), fclose);
    Memory_t vmPeak_Kb = 0;
	char buf[256];

	while (!feof(fp.get())) {
        if (fgets(buf, 256, fp.get()) == NULL) {
            throw std::runtime_error("Error, can't get the VmPeakSize");
		}

        if (::strncmp(buf, "VmPeak:", 7))
			continue;
        ::sscanf(buf, "%*s %lu", &vmPeak_Kb);
		return vmPeak_Kb ;
	}

	return vmPeak_Kb;
}

} // namespace monitor

} // namespace argo
