/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/monitor/time_monitor.h"

#include <ratio>
#include <stdexcept>




using std::chrono::duration_cast;
using std::chrono::duration;

namespace argo {
namespace monitor {


Time_t getElapsedTimeNanoseconds(std::chrono::steady_clock::time_point tStart, std::chrono::steady_clock::time_point tStop) {
    Time_t elapsedTime = duration_cast<std::chrono::nanoseconds>(tStop - tStart).count();
    return elapsedTime;
}

Time_t getElapsedTimeMicroseconds(std::chrono::steady_clock::time_point tStart, std::chrono::steady_clock::time_point tStop) {
    Time_t elapsedTime = duration_cast<std::chrono::microseconds>(tStop - tStart).count();
    return elapsedTime;
}

Time_t getElapsedTimeMilliseconds(std::chrono::steady_clock::time_point tStart, std::chrono::steady_clock::time_point tStop) {
    Time_t elapsedTime = duration_cast<std::chrono::milliseconds>(tStop - tStart).count();
    return elapsedTime;
}

Time_t getElapsedTimeSeconds(std::chrono::steady_clock::time_point tStart, std::chrono::steady_clock::time_point tStop) {
    Time_t elapsedTime = duration_cast<std::chrono::seconds>(tStop - tStart).count();
    return elapsedTime;
}




TimeMonitor::TimeMonitor(TimeMeasure time_measure, WindowSize window_size):DataBuffer<Time_t>(window_size) {

    switch(time_measure) {
    case TimeMeasure::Microseconds:
        time_extractor = getElapsedTimeMicroseconds;
        break;
    case TimeMeasure::Milliseconds:
        time_extractor = getElapsedTimeMilliseconds;
        break;
    case TimeMeasure::Seconds:
        time_extractor = getElapsedTimeSeconds;
        break;
    case TimeMeasure::Nanoseconds:
        time_extractor = getElapsedTimeNanoseconds;
    default:
        throw std::logic_error("DEFENSIVE PROGRAMMING: Undefined TimeMeasure in time monitor");
    }


    started = false;

}



TimeMonitor::TimeMonitor(WindowSize window_size):DataBuffer<Time_t>(window_size) {


    time_extractor = getElapsedTimeMilliseconds;

    started = false;
}


void TimeMonitor::start() {

    if (started) {
        return;
    }

    started = true;

    tStart = std::chrono::steady_clock::now();

}


void TimeMonitor::stop() {

    if (!started) {
        return;
    }

    Time_t time_elapsed = time_extractor(tStart, std::chrono::steady_clock::now());


    push(time_elapsed);

    started = false;

}




} // namespace monitor

} // namespace argo
