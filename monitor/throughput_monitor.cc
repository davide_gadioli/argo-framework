/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/monitor/throughput_monitor.h"
#include <stdexcept>





using std::chrono::duration_cast;
using std::chrono::duration;

namespace argo {
namespace monitor {
  

ThroughputMonitor::ThroughputMonitor(WindowSize window_size):DataBuffer<Throughput_t>(window_size) {
    started = false;
}



void ThroughputMonitor::start() {

    if (started) {
        return;
    }

    tStart = std::chrono::steady_clock::now();

    started = true;
}


void ThroughputMonitor::stop(Data_elaborated_t data) {

    std::chrono::steady_clock::time_point tStop = std::chrono::steady_clock::now();

    if (!started) {
        return;

    }

    Time_t elapsedTime = duration_cast<std::chrono::microseconds>(tStop - tStart).count();

    push(data * (1000000.0f / elapsedTime));

    started = false;
}








} // namespace monitor

} // namespace argo
