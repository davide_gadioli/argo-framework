/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/monitor/system_cpu_usage_monitor.h"
#include <string>
#include <stdexcept>
#include <unistd.h>




namespace argo {
namespace monitor {

inline void readTime(Time_t* user, Time_t* nice, Time_t* system, Time_t* idle) {

    std::shared_ptr<FILE> fp(fopen("/proc/stat", "r"), fclose);
    Time_t io_wait, irq, softirq, steal, guest, guest_nice = 0;
    int result = ::fscanf(fp.get(), "%*s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu", user, nice, system, idle, &io_wait, &irq, &softirq, &steal, &guest, &guest_nice);

    system += irq + softirq + steal + guest + guest_nice + io_wait;

    // check if it's correct
    if (result == EOF) {
        throw std::runtime_error("Error, can't parse the /proc/stat file: reached end of file.");
    }
}

inline void checkTotal(Time_t total) {

    if (total == 0) {
        throw std::runtime_error("Sys percentage error: no time elapsed (interval too short?)");
    }
}


SystemCpuUsageMonitor::SystemCpuUsageMonitor(WindowSize window_size):DataBuffer<CpuUsage_t>(window_size) {
     started = false;
}



void SystemCpuUsageMonitor::start() {

    if(started) {
        return;
    }

    started = true;

    // save the current measure
    Time_t user,sys,nice,idle;
    readTime(&user, &nice, &sys, &idle);
    busy_time = user + sys + nice;
    total_time = busy_time + idle;

}



void SystemCpuUsageMonitor::stop() {
    // get the new measure
    Time_t user, sys, nice, idle, total, busy;
    readTime(&user, &nice, &sys, &idle);
    busy = user + sys + nice;
    total = busy + idle;

    if (!started) {
        return;

    }

    started = false;

    // subtract the previous one
    total -= total_time;
    busy -= busy_time;
    checkTotal(total);

    // push the cpu usage into the
    CpuUsage_t percentage = ((CpuUsage_t) busy)/((CpuUsage_t) total);

    push(percentage * sysconf( _SC_NPROCESSORS_ONLN ));
}




} // namespace monitor

} // namespace argo
